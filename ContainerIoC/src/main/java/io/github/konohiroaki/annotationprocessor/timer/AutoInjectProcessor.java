package io.github.konohiroaki.annotationprocessor.timer;

import com.google.auto.service.AutoService;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeTranslator;

import com.sun.source.util.Trees;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.util.Set;

@SupportedAnnotationTypes("com.epam.cloud.annotation.AutoInject")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor.class)
public class AutoInjectProcessor extends AbstractProcessor {

    private Trees trees;
    private TreeTranslator visitor;

    @Override
    public void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        processingEnvironment.getMessager().printMessage(Diagnostic.Kind.NOTE, "-ghj-ghj-fgjf-gjfj-fgj-fg-hj-fgh-jfghj-fgj-fghjfg-hj-fg-hj-fgh-jf-g");
        trees = Trees.instance(processingEnvironment);
        visitor = new TimerTranslator(((JavacProcessingEnvironment) processingEnvironment).getContext());
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment) {
        if (!roundEnvironment.processingOver()) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "----------------------------errrrroooorrrr-------------");
            Set<? extends Element> elements = roundEnvironment.getRootElements();
            elements.forEach(element -> {
                JCTree tree = (JCTree) trees.getTree(element);
                tree.accept(visitor);
            });
            return false;
        }
        return true;
    }
}