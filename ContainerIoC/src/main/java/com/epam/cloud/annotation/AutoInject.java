package com.epam.cloud.annotation;

import com.epam.cloud.enums.ObjectScope;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface AutoInject {
    ObjectScope scope() default ObjectScope.SINGLETON;
}
