package com.epam.cloud.container;

import com.epam.cloud.annotation.AutoInject;
import com.epam.cloud.annotation.NeedAdditionalInitialization;
import com.epam.cloud.enums.ObjectScope;
import com.epam.cloud.exceptions.PrototypeNotSupportedException;
import com.epam.cloud.exceptions.ScoupNotSupportedException;
import com.epam.cloud.interfaces.Cloneable;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Container {
    private static Map<String, ObjectHandler> objectMap = new HashMap<>();

    public void initContainer() {
        Map<ObjectHandler, Boolean> typesToAddMap = new HashMap<>();
        typesToAddMap.put(new ObjectHandler("com.epam.cloud.mypack.MyClass1"), false);
        typesToAddMap.put(new ObjectHandler("com.epam.cloud.mypack.MyClass2"), false);
        typesToAddMap.put(new ObjectHandler("com.epam.cloud.mypack.MyClass3"), false);
        for (Map.Entry<ObjectHandler, Boolean> entry : typesToAddMap.entrySet()) {
            boolean loaded = entry.getValue();
            if (!loaded) {
                addTypeToMap(entry.getKey(), typesToAddMap);
            }
        }
    }

    public static Object getByType(String type) {
        ObjectHandler resultObj = objectMap.get(type);
        if (resultObj != null) {
            if (resultObj.scope == ObjectScope.SINGLETON) {
                return resultObj.obj;
            } else {
                if (resultObj.scope == ObjectScope.PROTOTYPE) {
                    if (resultObj.obj instanceof Cloneable) {
                        return getClone((Cloneable) resultObj.obj);
                    } else {
                        throw new PrototypeNotSupportedException();
                    }
                } else {
                    throw new ScoupNotSupportedException();
                }
            }
        } else {
            throw new IllegalArgumentException();
        }
    }


    private void addTypeToMap(ObjectHandler objectHandler, Map<ObjectHandler, Boolean> typesToAddMap) {
        try {
            Class clazz = Class.forName(objectHandler.type);
            System.out.println("Loading " + clazz.getSimpleName());
            Constructor constructor = clazz.getConstructor();
            Object newObject = constructor.newInstance();
            objectHandler.obj = newObject;
            ObjectHandler oldObjectHandler1 = objectMap.put(objectHandler.type, objectHandler);
            if (oldObjectHandler1 != null) {
                throw new IllegalArgumentException();
            }
            typesToAddMap.put(objectHandler, true);
            checkClassFields(objectHandler, typesToAddMap);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }


    private static <T> T getClone(Cloneable<T> cloneableObject) {
        return cloneableObject.getObjectClone();
    }

    private void checkClassFields(ObjectHandler objectHandler, Map<ObjectHandler, Boolean> typesToAddMap)
            throws IllegalAccessException, NoSuchMethodException, InvocationTargetException,
            InstantiationException, NoSuchFieldException {
        Class clazz = objectHandler.obj.getClass();
        do {
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                checkFieldForAutoInject(objectHandler, typesToAddMap, field);
                checkFieldToAddInit(objectHandler, field);
            }
            clazz = clazz.getSuperclass();
        } while (clazz != null);
    }

    private void checkFieldToAddInit(ObjectHandler objectHandler, Field field) throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException {
        if (field.isAnnotationPresent(NeedAdditionalInitialization.class)) {
            NeedAdditionalInitialization annotation = field.getAnnotation(NeedAdditionalInitialization.class);
            Class initializationClass = annotation.initClass();
            Object targetObject = field.get(objectHandler.obj);
            if (targetObject == null) {
                targetObject = field.getType().getConstructor().newInstance();
            }
            for (Field initObjectField : initializationClass.getDeclaredFields()) {
                initObjectField.setAccessible(true);
                Field objField = targetObject.getClass().getDeclaredField(initObjectField.getName());
                objField.setAccessible(true);
                objField.set(targetObject, initObjectField.get(null));
            }
            field.set(objectHandler.obj, targetObject);
        }
    }

    private void checkFieldForAutoInject(ObjectHandler objectHandler, Map<ObjectHandler, Boolean> typesToAddMap, Field field) throws IllegalAccessException {
        if (field.isAnnotationPresent(AutoInject.class)) {
            ObjectHandler objectHandler1FromMap = objectMap.get(field.getType().getName());
            if (objectHandler1FromMap != null) {
                field.set(objectHandler.obj, objectHandler1FromMap.obj);
            } else {
                addTypeToMap(new ObjectHandler(field.getType().getName()), typesToAddMap);
                objectHandler1FromMap = objectMap.get(field.getType().getName());
                field.set(objectHandler.obj, objectHandler1FromMap.obj);
            }
        }
    }

    static class ObjectHandler {
        private Object obj;
        private String type;
        private ObjectScope scope;

        public ObjectHandler(String type, ObjectScope scope) {
            this.type = type;
            this.scope = scope;
        }

        public ObjectHandler(String type) {
            this.type = type;
            this.scope = ObjectScope.SINGLETON;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ObjectHandler that = (ObjectHandler) o;
            return Objects.equals(type, that.type);
        }

        @Override
        public int hashCode() {
            return type.hashCode();
        }
    }
}
