package com.epam.cloud.mypack;

import com.epam.cloud.annotation.AutoInject;

public class MyClass4 {
    private String testString;
    private int intParam;
    private Object myObject;

    public MyClass4() {
    }

    @AutoInject
    public void out() {
        System.out.println("String ->" + testString + ", intParam ->" + intParam + ", myObject ->" + myObject);
    }

    @Override
    public String toString() {
        return "String ->" + testString + ", intParam ->" + intParam + ", myObject ->" + myObject;
    }
}
