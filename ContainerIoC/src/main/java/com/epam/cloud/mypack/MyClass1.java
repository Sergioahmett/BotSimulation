package com.epam.cloud.mypack;

import com.epam.cloud.annotation.AutoInject;
import com.epam.cloud.annotation.NeedAdditionalInitialization;

public class MyClass1 {
//    @AutoInject
    MyClass2 clazz2;
    @NeedAdditionalInitialization(initClass = InitClass.class)
    MyClass4 clazz4;

    public void run() {
        System.out.println("Hi from class 1");
        System.out.println(clazz2.getClass1() == this);
        clazz2.run();
    }

    public void out() {
        System.out.println("Exit from class 1");
        clazz4.out();
    }

    public void neww() {
        System.out.println("neww in class 1");
    }

    public MyClass2 getClazz2() {
        return clazz2;
    }

    public void setClazz2(MyClass2 clazz2) {
        this.clazz2 = clazz2;
    }
}
