package com.epam.cloud.mypack;

import com.epam.cloud.annotation.AutoInject;

public class MyClass3 {
//    @AutoInject
    private MyClass1 class1;
//    @AutoInject
    private MyClass2 class2;

    public void run() {
        System.out.println("Hi from class 3");
        class1.neww();
        class2.neww();
    }

    public MyClass1 getClass1() {
        return class1;
    }

    public void setClass1(MyClass1 class1) {
        this.class1 = class1;
    }

    public MyClass2 getClass2() {
        return class2;
    }

    public void setClass2(MyClass2 class2) {
        this.class2 = class2;
    }
}
