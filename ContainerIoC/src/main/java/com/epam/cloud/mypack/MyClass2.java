package com.epam.cloud.mypack;

import com.epam.cloud.annotation.AutoInject;

public class MyClass2 {

//    @AutoInject
    private MyClass1 class1;
//    @AutoInject
    private MyClass3 class3;

    public void run() {
        System.out.println("Hi from class 2");
        class1.out();
        class3.run();
    }

    public void neww() {
        System.out.println("neww in class 2");
    }

    public MyClass1 getClass1() {
        return class1;
    }

    public void setClass1(MyClass1 class1) {
        this.class1 = class1;
    }

    public MyClass3 getClass3() {
        return class3;
    }

    public void setClass3(MyClass3 class3) {
        this.class3 = class3;
    }
}
