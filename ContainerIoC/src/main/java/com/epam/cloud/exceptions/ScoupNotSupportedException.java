package com.epam.cloud.exceptions;

public class ScoupNotSupportedException extends RuntimeException {
    public ScoupNotSupportedException() {
    }

    public ScoupNotSupportedException(String message) {
        super(message);
    }
}
