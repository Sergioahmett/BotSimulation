package com.epam.cloud.exceptions;

public class PrototypeNotSupportedException extends RuntimeException {
    public PrototypeNotSupportedException() {
    }

    public PrototypeNotSupportedException(String message) {
        super(message);
    }
}
