package com.epam.cloud;

import com.epam.cloud.container.Container;
import com.epam.cloud.interfaces.RunnableClass;
import com.epam.cloud.mypack.MyClass1;

public class Starter implements RunnableClass {
    @Override
    public void run() {
        System.out.println("Run success.");
        ((MyClass1) Container.getByType(MyClass1.class.getName())).run();
        System.out.println("Test success.");
    }
}
