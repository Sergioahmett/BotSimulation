package com.epam.cloud.enums;

public enum ObjectScope {
    SINGLETON, PROTOTYPE
}
