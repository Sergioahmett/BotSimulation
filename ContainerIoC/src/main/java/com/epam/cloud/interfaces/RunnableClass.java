package com.epam.cloud.interfaces;

public interface RunnableClass {
    void run();
}
