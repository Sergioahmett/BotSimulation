package com.epam.cloud.interfaces;

public interface Cloneable<T> extends java.lang.Cloneable {
    T getObjectClone();
}
