package com.epam.cloud.starter;


import com.epam.cloud.container.Container;
import com.epam.cloud.interfaces.RunnableClass;
import io.github.konohiroaki.annotationprocessor.timer.Timer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Application {
    @Timer({3, 6})
    public static void startApp(Class<? extends RunnableClass> clazz) {
        Container container = new Container();
        container.initContainer();
        Constructor constructor = null;
        try {
            constructor = clazz.getConstructor();
            Object newObject = constructor.newInstance();
            ((RunnableClass) newObject).run();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Application.startApp(null);
    }
}
