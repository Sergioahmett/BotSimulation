package gui;

import java.awt.*;
import java.util.Arrays;

import javax.swing.*;
import mainlogic.SimulationPanel;

public class SimulationFrame extends JFrame { 

	private static final long serialVersionUID = 1L;
	private SimulationPanel mainPanel = null;
	private JButton startButton = null;
	private JButton newSimulationButton = null;
	private JButton displayButton = null;
	private JSlider updateDelaySlider = null;
	private JLabel maxLifeTimeLable = null;
	private JLabel currentGenerationNumberLable = null;
	private JLabel[] generationLifetimeLabels = new JLabel[10];
	private JLabel currentGenerationLifeLable = null;
	private JCheckBox outputChartFlag = null;
	private JCheckBox outputResultFlag = null;

	public SimulationFrame(int width, int height, String title) {
		super(title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		createAndStartSimulationPanel(width, height);
		createUpperToolbar();
		createRightToolbar();
		createLowerToolbar();
		createListeners();
		pack();
		setVisible(true);
		mainPanel.setUpdateDelay(updateDelaySlider.getValue());
	}

	public boolean getOutputChartFlag() {
		return outputChartFlag.isSelected();
	}

	public boolean getOutputResultFlag() {
		return outputResultFlag.isSelected();
	}

	public void setMaxLifeTimeLable(String str) {
		maxLifeTimeLable.setText(str);
	}

	public void setCurrentGenerationNumberLable(String str) {
		currentGenerationNumberLable.setText(str);
	}

	public void setGenerationLifetimeLabels(String str) {
		for (int i = generationLifetimeLabels.length - 1; i > 0; i--) {
			generationLifetimeLabels[i].setText(generationLifetimeLabels[i - 1].getText());
		}
		generationLifetimeLabels[0].setText(str);
	}

	public void resetLifeLabels() {
		Arrays.stream(generationLifetimeLabels).forEach(e -> e.setText("0"));
	}

	public void setCurrentGenerationLifeLable(String str) {
		if (mainPanel.isWithGraph()) {
			currentGenerationLifeLable.setText("Generation life: " + str);
		} else {
			currentGenerationLifeLable.setText("Generation life: ---");
		}
	}

	private void createGenerationLifetimeLable(JPanel lifeTimesOld) {
		for (int i = 0; i < generationLifetimeLabels.length; i++) {
			generationLifetimeLabels[i] = new JLabel("0");
			lifeTimesOld.add(generationLifetimeLabels[i]);
		}
	}

	private void createUpperToolbar() {
		JToolBar upperToolbar = new JToolBar();
		upperToolbar.setFloatable(false);
		add(upperToolbar, BorderLayout.NORTH);

		outputChartFlag = new JCheckBox("Chart output");
		upperToolbar.add(outputChartFlag);

		outputResultFlag = new JCheckBox("Save result");
		upperToolbar.add(outputResultFlag);

		upperToolbar.addSeparator();

		JLabel sliderLabel1 = new JLabel("Faster");
		upperToolbar.add(sliderLabel1);

		updateDelaySlider = new JSlider(5, 200);
		updateDelaySlider.setValue(50);
		updateDelaySlider.setMaximumSize(new Dimension(350, 50));
		upperToolbar.add(updateDelaySlider);

		JLabel sliderLabel2 = new JLabel("Slower");
		upperToolbar.add(sliderLabel2);
	}

	private void createAndStartSimulationPanel(int width, int height) {
		mainPanel = new SimulationPanel();
		mainPanel.initialize(width, height, this); 
		add(mainPanel);
		setSize(mainPanel.getPreferredSize());
	}

	private void createRightToolbar() {
		JToolBar rightToolbar = new JToolBar();
		rightToolbar.setLayout(null);
		rightToolbar.setFloatable(false);
		rightToolbar.setPreferredSize(new Dimension(200, 60));
		add(rightToolbar, BorderLayout.EAST);

		JPanel maxLifePanel = new JPanel(new FlowLayout());
		maxLifePanel.setBorder(BorderFactory.createTitledBorder("      Max life time"));
		maxLifePanel.setSize(new Dimension(200, 50));
		maxLifePanel.setLocation(0, 0);
		maxLifeTimeLable = new JLabel("0");
		maxLifePanel.add(maxLifeTimeLable);
		rightToolbar.add(maxLifePanel);

		JPanel currentGenerationPanel = new JPanel(new FlowLayout());
		currentGenerationPanel.setBorder(BorderFactory.createTitledBorder("      Current generation"));
		currentGenerationPanel.setSize(new Dimension(200, 50));
		currentGenerationPanel.setLocation(0, 55);
		currentGenerationNumberLable = new JLabel("0");
		currentGenerationPanel.add(currentGenerationNumberLable);
		rightToolbar.add(currentGenerationPanel);

		JPanel lifeTimesOld = new JPanel(new FlowLayout());
		lifeTimesOld.setLayout(new BoxLayout(lifeTimesOld, BoxLayout.Y_AXIS));
		lifeTimesOld.setBorder(BorderFactory.createTitledBorder("Last generation lifetime:"));
		lifeTimesOld.setSize(new Dimension(200, 175));
		lifeTimesOld.setLocation(0, 110);
		createGenerationLifetimeLable(lifeTimesOld);
		rightToolbar.add(lifeTimesOld);

		startButton = new JButton("Start");
		startButton.setBorder(BorderFactory.createBevelBorder(0));
		startButton.setSize(150, 50);
		startButton.setLocation(25, 350);
		rightToolbar.add(startButton);

		displayButton = new JButton("Enable display");
		displayButton.setBorder(BorderFactory.createBevelBorder(0));
		displayButton.setSize(150, 25);
		displayButton.setLocation(25, 320);
		rightToolbar.add(displayButton);

		newSimulationButton = new JButton("New simulation start");
		newSimulationButton.setBorder(BorderFactory.createBevelBorder(0));
		newSimulationButton.setSize(150, 25);
		newSimulationButton.setLocation(25, 290);
		rightToolbar.add(newSimulationButton);
	}

	private void createLowerToolbar() {
		JToolBar lowerToolbar = new JToolBar();
		lowerToolbar.setFloatable(false);
		add(lowerToolbar, BorderLayout.SOUTH);
		currentGenerationLifeLable = new JLabel("Generation lifetime: ");
		currentGenerationLifeLable.setMinimumSize(new Dimension(150, 25));
		lowerToolbar.add(currentGenerationLifeLable);
	}

	private void createListeners() {
		updateDelaySlider.addChangeListener(e -> mainPanel.setUpdateDelay(updateDelaySlider.getValue()));

		startButton.addActionListener(e -> {
			if (mainPanel.isSimulating()) {
				mainPanel.stopSimulation();
				startButton.setText("Start");
			} else {
				mainPanel.startSimulation();
				startButton.setText("Stop");
			}
		});

		newSimulationButton.addActionListener(e -> {
			mainPanel.stopSimulation();
			try {
				mainPanel.getCurentThread().join();
			} catch (InterruptedException|NullPointerException e1) {
				System.out.println(e1.getMessage());
			}
			mainPanel.getLifeModel().startNewStepSimul();
			mainPanel.startSimulation();
			mainPanel.repaint();
		});

		displayButton.addActionListener(e -> {
			if (mainPanel.isWithGraph()) {
				mainPanel.setWithGraph(false);
				displayButton.setText("Enable display");
			} else {
				mainPanel.setWithGraph(true);
				displayButton.setText("Disenable diplay");
			}
		});
	}
}
