package entityclasses;

import java.awt.Color;
import java.util.Random;

import constants.MyConstants;
import enums.ToDoEnum;

public class Bot {
	private Color color = MyConstants.COLOR_OF_BOT;
	private int x;
	private int y;
	private int[] brain;
	private int tempBite;
	private int view;
	private int energy;
	private ToDoClass toDo;

	public Bot(int x, int y) {
		this.x = x;
		this.y = y;
		Random r = new Random();
		this.brain = new int[64];
		setRandomBrein();
		this.tempBite = 0;
		this.view = r.nextInt(8);
		this.energy = MyConstants.STARTING_BOTS_ENERGY;
		this.toDo = new ToDoClass(ToDoEnum.empty, 0, 0);
	}

	public Bot(int x, int y, int[] arr) {
		this.x = x;
		this.y = y;
		Random r = new Random();
		this.brain = arr;
		this.tempBite = 0;
		this.view = r.nextInt(8);
		this.energy = MyConstants.STARTING_BOTS_ENERGY;
		this.toDo = new ToDoClass(ToDoEnum.empty, 0, 0);
	}

	public int getMoveDirection() {
		int k = this.view + this.brain[tempBite];
		while (k > 7) {
			k = k - 8;
		}
		return k;
	}

	public int getLookDirection() {
		int k = this.view + this.brain[tempBite];

		while (k > 31) {
			k = k - 8;
		}
		return k;
	}

	public int getTakeDirection() {
		int k = this.view + this.brain[tempBite];
		while (k > 23) {
			k = k - 8;
		}
		return k;
	}

	public String toString() {
		String temp = "Brain: ";
		for (int i = 0; i < brain.length; i++) {
			temp = temp + brain[i] + ", ";
		}
		return temp;
	}

	public void setRandomBrein() {
		Random r = new Random();
		for (int i = 0; i < brain.length; i++) {
			brain[i] = r.nextInt(64);
		}
	}

	public void paint(Cell[][] cell1, Bot bote) {
		cell1[x][y] = new Cell("Bot");
		cell1[x][y].setColor(bote.getColor());
	}

	public Color getColor() {
		return this.color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int[] getBrain() {
		return brain;
	}

	public void setBrain(int[] brain) {
		this.brain = brain;
	}

	public int getView() {
		return view;
	}

	public void setView(int view) {
		this.view = view;
	}

	public int getTempBite() {
		return tempBite;
	}

	public void setTempBite(int tempBite) {
		this.tempBite = tempBite;
	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int energy) {
		this.energy = energy;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public ToDoClass getToDo() {
		return toDo;
	}

	public void setToDo(ToDoEnum toDo, int x, int y) {
		this.toDo.setTodo(toDo);
		this.toDo.setXY(x, y);
	}

}
