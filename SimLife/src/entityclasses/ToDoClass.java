package entityclasses;

import enums.ToDoEnum;

public class ToDoClass {
	private int x;
	private int y;
	private ToDoEnum todo;

	ToDoClass(ToDoEnum todo, int x, int y) {
		this.setTodo(todo);
		this.setX(x);
		this.setY(y);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setXY(int x, int y) {
		this.y = y;
		this.x = x;
	}

	public ToDoEnum getTodo() {
		return todo;
	}

	public void setTodo(ToDoEnum todo) {
		this.todo = todo;
	}

}
