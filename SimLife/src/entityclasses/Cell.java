package entityclasses;

import java.awt.Color;

import constants.MyConstants;

public class Cell {
	private String name;
	private Color color;

	public Cell(String name) {
		this.name = name;
		switch (name) {
		case "Bot":
			this.color = MyConstants.COLOR_OF_BOT;
			break;
		case "Food":
			this.color = MyConstants.COLOR_OF_FOOD;
			break;
		case "Wall":
			this.color = MyConstants.COLOR_OF_WALL;
			break;
		case "Poison":
			this.color = MyConstants.COLOR_OF_POISON;
			break;
		case "Empty":
			this.color = MyConstants.COLOR_OF_EMPTY;
			break;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setCell(Cell cell) {
		this.name = cell.getName();
		this.color = cell.getColor();
	}
}
