package mainlogic;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.WindowConstants;

import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.BitmapEncoder.BitmapFormat;

import constants.MyConstants;
import entityclasses.Bot;
import entityclasses.Cell;
import virtualmachine.VirtualMachine;

import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;

public class CellFieldLogic {
	private SimulationPanel upperPanel;
	private Cell[][] cellsField;
	private Bot[] botsArray;
	private int width;
	private int high;
	private int countOfFood = 0;
	private int countOfPoison = 0;
	private boolean nextGen = false;
	private int genNomber = 1;
	private int lifeTime = 0;
	private int maxLifeTime = 0;
	private PrintWriter writer;

	private ArrayList<Double> xData = new ArrayList<>(30000);
	private ArrayList<Double> yData = new ArrayList<>(30000);
	private int SimulCounter = 1;
	private VirtualMachine VM;

	public CellFieldLogic(int width, int high, SimulationPanel upperPanel) {
		this.width = width;
		this.high = high;
		this.upperPanel = upperPanel;
		botsArray = new Bot[MyConstants.MAX_BOTS_NUMBER];
		cellsField = newEmptyField(width, high);
		createWalls();
		addBots();
		foodGenerate();
		poisonGenerate();

	}

	public Cell[][] newEmptyField(int w, int h) {
		Cell[][] cellsField = new Cell[h][w];
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				cellsField[i][j] = new Cell("Empty");
			}
		}
		return cellsField;
	}

	public void addBots() {
		Random r = new Random();
		boolean flag;
		if (0 < botsArray.length) {
			for (int i = 0; i < botsArray.length; i++) {
				flag = true;
				while (flag) {
					int x = r.nextInt(high);
					int y = r.nextInt(width);
					if (cellsField[x][y].getName() == "Empty") {
						botsArray[i] = new Bot(x, y);
						botsArray[i].paint(cellsField, botsArray[i]);
						flag = false;
					}
				}
			}
		}
	}

	public void createWalls() {
		for (int i = 0; i < high; i++) {
			cellsField[i][0].setCell(new Cell("Wall"));
			cellsField[i][width - 1].setCell(new Cell("Wall"));
		}
		for (int i = 0; i < width; i++) {
			cellsField[0][i].setCell(new Cell("Wall"));
			cellsField[high - 1][i].setCell(new Cell("Wall"));
		}

		for (int i = 0; i < (high / 2.5); i++) {
			cellsField[i][(width / 3)].setCell(new Cell("Wall"));
			cellsField[high - i - 1][width / 3 * 2].setCell(new Cell("Wall"));
		}

	}

	public void foodGenerate() {
		Random r = new Random();

		boolean flag;
		if (countOfFood < MyConstants.MAX_FOOD_NUMBER) {
			for (int i = countOfFood; i < MyConstants.MAX_FOOD_NUMBER; i++) {
				flag = true;
				while (flag) {
					int x = r.nextInt(high);
					int y = r.nextInt(width);
					if (cellsField[x][y].getName() == "Empty") {
						cellsField[x][y].setCell(new Cell("Food"));
						flag = false;
					}
				}
				countOfFood++;
			}
		}
	}

	public void poisonGenerate() {
		Random r = new Random();
		boolean flag;
		if (countOfPoison < MyConstants.MAX_POISON_NUMBER) {
			for (int i = countOfPoison; i < MyConstants.MAX_POISON_NUMBER; i++) {
				flag = true;
				while (flag) {
					int x = r.nextInt(high);
					int y = r.nextInt(width);
					if (cellsField[x][y].getName() == "Empty") {
						cellsField[x][y].setCell(new Cell("Poison"));
						flag = false;
					}
				}
				countOfPoison++;
			}
		}
	}

	public void simulate() {
		if (nextGen) {
			doNextGen();
			nextGen = false;
		}
		VM = new VirtualMachine(cellsField, botsArray);
		VM.go();
		botsArray = VM.getTempBotArr();
		lifeTime++;
		lifeTimeOutput();

		if (botsArray.length == 8) {
			if (lifeTime > maxLifeTime) {
				maxLifeTime = lifeTime;
			}
			dataOutputToFrame();
			xData.add((double) genNomber);
			yData.add((double) lifeTime);
			genNomber++;
			nextGen = true;
		}
		checkFoodPoison();
		foodGenerate();
		poisonGenerate();

		if ((maxLifeTime > MyConstants.MAX_BOTS_LIFETIME) || (genNomber - 1 > 30000)) {
			resultToFile();
			doGraph();
			startNewStepSimul();
		}

	}

	public void dataOutputToFrame() {
		upperPanel.getUpperForm().setMaxLifeTimeLable(Integer.toString(maxLifeTime));
		upperPanel.getUpperForm().setCurrentGenerationNumberLable(Integer.toString(genNomber));
		upperPanel.getUpperForm().setGenerationLifetimeLabels(Integer.toString(lifeTime));
	}

	public void lifeTimeOutput() {
		upperPanel.getUpperForm().setCurrentGenerationLifeLable(Integer.toString(lifeTime));
	}

	public void resultToFile() {
		if (upperPanel.getUpperForm().getOutputResultFlag()) {
			try {
				writer = new PrintWriter(MyConstants.NAME_OF_OUTPUT_TEXT_FILE + SimulCounter + ".txt");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			writer.println("��������� ����� -> " + (genNomber - 1));
			writer.println();
			writer.println("������������ ������������ ����� -> " + maxLifeTime);
			writer.println();
			writer.println("������ �������� ��� ����:");
			for (int i = 0; i < botsArray.length; i++) {
				writer.println(botsArray[i]);
			}
			writer.close();
		}
	}

	public void doGraph() {
		XYChart chart = QuickChart.getChart("Gen/LifeTime", "GenNomber", "LifeTime", "y(x)", xData, yData);
		if (upperPanel.getUpperForm().getOutputChartFlag()) {
			new SwingWrapper<XYChart>(chart).displayChart().setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		}
		if (upperPanel.getUpperForm().getOutputResultFlag()) {
			try {
				BitmapEncoder.saveBitmap(chart, MyConstants.NAME_OF_OUTPUT_CHART_FILE + SimulCounter, BitmapFormat.PNG);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void checkFoodPoison() {
		countOfFood = 0;
		countOfPoison = 0;
		for (int y = 0; y < high; y++) {
			for (int x = 0; x < width; x++) {
				if (cellsField[y][x].getName() == "Food") {
					countOfFood++;
				}
				if (cellsField[y][x].getName() == "Poison") {
					countOfPoison++;
				}
			}
		}
	}

	public void doNextGen() {
		lifeTime = 0;
		if (upperPanel.isWithGraph()) {
			try {
				Thread.sleep(MyConstants.PAUSE_BEFORE_AUTOSTARTING_NEX_GENERATION);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		countOfFood = 0;
		countOfPoison = 0;
		cellsField = newEmptyField(width, high);
		createWalls();
		addNewBots();
		foodGenerate();
		poisonGenerate();

	}

	public void startNewStepSimul() {
		countOfFood = 0;
		countOfPoison = 0;
		nextGen = false;
		genNomber = 1;
		lifeTime = 0;
		maxLifeTime = 0;
		xData.clear();
		yData.clear();
		;
		botsArray = new Bot[MyConstants.MAX_BOTS_NUMBER];
		cellsField = newEmptyField(width, high);
		createWalls();
		addBots();
		foodGenerate();
		poisonGenerate();
		clearLabel();
		SimulCounter++;
	}

	public void clearLabel() {
		upperPanel.getUpperForm().resetLifeLabels();
		upperPanel.getUpperForm().setMaxLifeTimeLable(Integer.toString(0));
		upperPanel.getUpperForm().setCurrentGenerationNumberLable(Integer.toString(0));
	}

	public void addNewBots() {
		Random r = new Random();
		boolean flag;
		Bot[] tempBotsArr = new Bot[MyConstants.MAX_BOTS_NUMBER];
		int count2 = 0;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				flag = true;
				while (flag) {
					int x = r.nextInt(high);
					int y = r.nextInt(width);
					if (cellsField[x][y].getName() == "Empty") {
						if (j < 8 - MyConstants.NUMBER_OF_MUTANTS_FOR_EIGHT_BOTS) {
							tempBotsArr[count2] = new Bot(x, y, botsArray[i].getBrain());
						} else {
							tempBotsArr[count2] = new Bot(x, y, mutate(botsArray[i].getBrain()));
						}
						tempBotsArr[count2].paint(cellsField, tempBotsArr[count2]);
						count2++;
						flag = false;
					}
				}
			}
		}
		botsArray = tempBotsArr;
	}

	public int[] mutate(int[] b) {
		Random r = new Random();
		int[] x = b.clone();
		for (int i = 0; i < MyConstants.NUMBER_OF_MUTATIONS_IN_ONE_MUTANT; i++) {
			x[r.nextInt(64)] = r.nextInt(64);
		}
		return x;

	}

	public Cell getCells(int i, int j) {
		return cellsField[i][j];
	}

	public int getWidth() {
		return width;
	}

	public int getHigh() {
		return high;
	}

}
