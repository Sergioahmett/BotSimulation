package mainlogic;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.JPanel;

import constants.MyConstants;
import gui.SimulationFrame;

public class SimulationPanel extends JPanel implements Runnable {
	private static final long serialVersionUID = 790625337814794295L;
	private Thread simThread = null;
	private Thread curentThread = null;
	private SimulationFrame upperForm;
	private CellFieldLogic life = null;
	private boolean withGraph = true;
	private int updateDelay = 50;

	public SimulationPanel() {
		setBackground(MyConstants.COLOR_OF_EMPTY);
	}

	public CellFieldLogic getLifeModel() {
		return life;
	}

	public void initialize(int width, int height, SimulationFrame upperForm) {
		life = new CellFieldLogic(width, height, this);
		this.upperForm = upperForm;
	}

	public void setUpdateDelay(int updateDelay) {
		this.updateDelay = updateDelay;
	}

	public SimulationFrame getUpperForm() {
		return upperForm;
	}

	public void startSimulation() {
		if (simThread == null) {
			simThread = new Thread(this);
			curentThread = simThread;
			simThread.start();
		}
	}

	public void stopSimulation() {
		simThread = null;

	}

	public boolean isSimulating() {
		return simThread != null;
	}

	public Thread getCurentThread() {
		return curentThread;
	}

	@Override
	public void run() {
		repaint();
		while (simThread != null) {
			if (withGraph) {
				try {
					Thread.sleep(updateDelay);
				} catch (InterruptedException e) {
				}
				life.simulate();
				repaint();
			} else {
				life.simulate();
			}
		}
		repaint();
	}

	@Override
	public Dimension getPreferredSize() {
		if (life != null) {
			Insets b = getInsets();
			return new Dimension(
					(MyConstants.CELL_SIZE + MyConstants.CELL_GAP) * life.getWidth() + MyConstants.CELL_GAP + b.left
							+ b.right,
					(MyConstants.CELL_SIZE + MyConstants.CELL_GAP) * life.getHigh() + MyConstants.CELL_GAP + b.top
							+ b.bottom);
		} else
			return new Dimension(100, 100);
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (life != null) {
			synchronized (life) {
				super.paintComponent(g);
				Insets b = getInsets();
				for (int y = 0; y < life.getHigh(); y++) {
					for (int x = 0; x < life.getWidth(); x++) {
						g.setColor(life.getCells(y, x).getColor());
						g.fillRect(b.left + MyConstants.CELL_GAP + x * (MyConstants.CELL_SIZE + MyConstants.CELL_GAP),
								b.top + MyConstants.CELL_GAP + y * (MyConstants.CELL_SIZE + MyConstants.CELL_GAP),
								MyConstants.CELL_SIZE, MyConstants.CELL_SIZE);
					}
				}
			}
		}
	}

	public boolean isWithGraph() {
		return withGraph;
	}

	public void setWithGraph(boolean withGraph) {
		this.withGraph = withGraph;
	}
}
