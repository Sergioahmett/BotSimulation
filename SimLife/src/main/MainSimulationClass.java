package main;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import constants.MyConstants;
import gui.SimulationFrame;

public class MainSimulationClass implements MyConstants {
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new SimulationFrame(MyConstants.FIELD_HIGH, MyConstants.FIELD_WIDTH, MyConstants.FRAME_NAME);
			}

		});
	}

}
