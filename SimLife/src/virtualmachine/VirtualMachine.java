package virtualmachine;

import constants.MyConstants;
import entityclasses.Bot;
import entityclasses.Cell;
import enums.ToDoEnum;

public class VirtualMachine {
	private Cell[][] cell;
	private Bot bot;
	private Bot[] botArr;
	private Bot[] arrayOfAliveBot;
	private int x;
	private int y;
	private boolean endMove;
	private int countOfSpin;
	private int countOfLook;
	private int countOfGoto;
	private int counter;

	private int aliveBots = 0;

	public VirtualMachine(Cell[][] cell, Bot[] botArr) {
		this.cell = cell;
		this.botArr = botArr;
		aliveBots = botArr.length;
	}

	public void go() {
		for (int i = 0; i < botArr.length; i++) {
			this.bot = botArr[i];
			this.x = bot.getX();
			this.y = bot.getY();
			this.endMove = false;
			if (bot.getEnergy() > 0) {
				counter = bot.getTempBite();
				countOfSpin = 0;
				countOfLook = 0;
				countOfGoto = 0;
				while (!endMove) {
					switch (bot.getBrain()[counter] / 8) {
					case 0:
						move();
						break;
					case 1:
						spin();
						break;
					case 2:
						take();
						break;
					case 3:
						look();
						break;
					default:
						goTo();
						break;
					}
				}
			} else {
				bot.setToDo(ToDoEnum.DIE, x, y);
				aliveBots--;
			}
			checkBotLife();
			paint();
			if (aliveBots == 8) {
				break;
			}

		}
		doArrayOfAliveBots();

	}

	public void doArrayOfAliveBots() {
		int diedBotsCounter = 0;
		for (int i = 0; i < botArr.length; i++) {
			if (botArr[i].getToDo().getTodo() == ToDoEnum.DIE) {
				diedBotsCounter++;
			}
		}
		arrayOfAliveBot = new Bot[botArr.length - diedBotsCounter];
		int counter = 0;
		for (int i = 0; i < botArr.length; i++) {
			if (botArr[i].getToDo().getTodo() != ToDoEnum.DIE) {
				arrayOfAliveBot[counter++] = botArr[i];
			}
		}
	}

	public Bot[] getTempBotArr() {
		return arrayOfAliveBot;
	}

	public void checkBotLife() {
		if (bot.getEnergy() > MyConstants.MAX_BOTS_ENERGY) {
			bot.setEnergy(MyConstants.MAX_BOTS_ENERGY);
		}
	}

	public void paint() {
		switch (bot.getToDo().getTodo()) {
		case STAY:
			cell[x][y] = new Cell("Bot");
			break;
		case GO:
			cell[bot.getToDo().getX()][bot.getToDo().getY()] = new Cell("Bot");
			cell[x][y] = new Cell("Empty");
			bot.setXY(bot.getToDo().getX(), bot.getToDo().getY());
			break;
		case TAKE:
			cell[x][y] = new Cell("Bot");
			if ((cell[bot.getToDo().getX()][bot.getToDo().getY()].getName() == "Food")) {
				cell[bot.getToDo().getX()][bot.getToDo().getY()] = new Cell("Empty");
			}
			if (cell[bot.getToDo().getX()][bot.getToDo().getY()].getName() == "Poison") {
				cell[bot.getToDo().getX()][bot.getToDo().getY()] = new Cell("Food");
			}
			break;
		case DIE:
			cell[x][y] = new Cell("Empty");
			break;
		default:
			break;
		}
	}

	private void goTo() {
		if (countOfGoto < 10) {
			int k = counter + bot.getBrain()[counter];
			if (k > 63) {
				k = k - 64;
			}
			counter = k;
			bot.setTempBite(counter);
			countOfGoto++;
		} else {
			bot.setEnergy(bot.getEnergy() - 1);
			bot.setToDo(ToDoEnum.STAY, x, y);
			endMove = true;
		}

	}

	private void look() {
		switch (bot.getLookDirection()) {
		case 24:
			takeLook(-1, -1);
			break;
		case 25:
			takeLook(-1, 0);
			break;
		case 26:
			takeLook(-1, 1);
			break;
		case 31:
			takeLook(0, -1);
			break;
		case 27:
			takeLook(0, 1);
			break;
		case 30:
			takeLook(1, -1);
			break;
		case 29:
			takeLook(1, 0);
			break;
		case 28:
			takeLook(1, 1);
			break;
		}

	}

	public void takeLook(int k, int p) {
		if (countOfLook < 10) {
			counterCheck(k, p);
			countOfLook++;
		} else {
			bot.setEnergy(bot.getEnergy() - 1);
			bot.setToDo(ToDoEnum.STAY, x, y);
			endMove = true;
		}

	}

	private void take() {
		switch (bot.getTakeDirection()) {
		case 16:
			takeCheck(-1, -1);
			break;
		case 17:
			takeCheck(-1, 0);
			break;
		case 18:
			takeCheck(-1, 1);
			break;
		case 23:
			takeCheck(0, -1);
			break;
		case 19:
			takeCheck(0, 1);
			break;
		case 22:
			takeCheck(1, -1);
			break;
		case 21:
			takeCheck(1, 0);
			break;
		case 20:
			takeCheck(1, 1);
			break;
		}
		endMove = true;
	}

	public void takeCheck(int k, int p) {
		switch (cell[x + k][y + p].getName()) {
		case "Bot":
		case "Wall":
		case "Empty":
			bot.setEnergy(bot.getEnergy() - 1);
			bot.setToDo(ToDoEnum.STAY, x, y);
			break;
		case "Food":
			bot.setEnergy(bot.getEnergy() + 10);
			bot.setToDo(ToDoEnum.TAKE, x + k, y + p);
			break;
		case "Poison":
			bot.setEnergy(bot.getEnergy() - 1);
			bot.setToDo(ToDoEnum.TAKE, x + k, y + p);
			break;
		}
		counterCheck(k, p);
	}

	private void spin() {
		if (countOfSpin < 10) {
			int k = bot.getView() + bot.getBrain()[counter];
			if (k > 7) {
				k = k - 8;
			}
			bot.setView(k);
			counter++;
			if (counter > 63) {
				counter = counter - 64;
			}
			bot.setTempBite(counter);
			countOfSpin++;
		} else {
			bot.setEnergy(bot.getEnergy() - 1);
			bot.setToDo(ToDoEnum.STAY, x, y);
			endMove = true;
		}

	}

	private void move() {
		switch (bot.getMoveDirection()) {
		case 0:
			moveChek(-1, -1);
			break;
		case 1:
			moveChek(-1, 0);
			break;
		case 2:
			moveChek(-1, 1);
			break;
		case 7:
			moveChek(0, -1);
			break;
		case 3:
			moveChek(0, 1);
			break;
		case 6:
			moveChek(1, -1);
			break;
		case 5:
			moveChek(1, 0);
			break;
		case 4:
			moveChek(1, 1);
			break;
		}
		endMove = true;

	}

	public void counterCheck(int k, int p) {
		switch (cell[x + k][y + p].getName()) {
		case "Bot":
			counter = counter + 3;
			break;
		case "Wall":
			counter = counter + 2;
			break;
		case "Empty":
			counter = counter + 5;
			break;
		case "Food":
			counter = counter + 4;
			break;
		case "Poison":
			counter = counter + 1;
			break;
		}
		if (counter > 63) {
			counter = counter - 64;
		}
		bot.setTempBite(counter);
	}

	public void moveChek(int k, int p) {
		switch (cell[x + k][y + p].getName()) {
		case "Bot":
		case "Wall":
			bot.setEnergy(bot.getEnergy() - 1);
			bot.setToDo(ToDoEnum.STAY, x, y);
			break;
		case "Empty":
			bot.setEnergy(bot.getEnergy() - 1);
			bot.setToDo(ToDoEnum.GO, x + k, y + p);
			break;
		case "Food":
			bot.setEnergy(bot.getEnergy() + 10);
			bot.setToDo(ToDoEnum.GO, x + k, y + p);
			break;
		case "Poison":
			bot.setEnergy(0);
			bot.setToDo(ToDoEnum.DIE, x, y);
			aliveBots--;
			break;
		}
		counterCheck(k, p);
	}

	public int getBotLife() {
		return aliveBots;
	}

	public void setLiveBots(int botLife) {
		this.aliveBots = botLife;
	}

}
