package constants;

import java.awt.Color;

public interface MyConstants {				
	public static final String FRAME_NAME = "Simulation of Life";		
	public static final int FIELD_HIGH = 50;							
	public static final int FIELD_WIDTH = 25;
	public static final String NAME_OF_OUTPUT_TEXT_FILE = "Simul_Test_�";			
	public static final int MAX_FOOD_NUMBER = 80;							
	public static final int MAX_POISON_NUMBER = 80;
	public static final int MAX_BOTS_NUMBER = 64;
	public static final int PAUSE_BEFORE_AUTOSTARTING_NEX_GENERATION = 1000;				
	public static final int MAX_BOTS_LIFETIME = 100000;						
	public static final int STARTING_BOTS_ENERGY = 35;								
	public static final int MAX_BOTS_ENERGY = 100;							
	public static final String NAME_OF_OUTPUT_CHART_FILE = "Chart_Test_�";			
	public static final int CELL_SIZE = 15;								
	public static final int CELL_GAP = 2;								
	public static final int NUMBER_OF_MUTANTS_FOR_EIGHT_BOTS = 2;							
	public static final int NUMBER_OF_MUTATIONS_IN_ONE_MUTANT = 2;	
	
	public static final Color COLOR_OF_BOT = Color.BLUE;
	public static final Color COLOR_OF_FOOD = Color.GREEN;
	public static final Color COLOR_OF_POISON = Color.RED;
	public static final Color COLOR_OF_EMPTY = Color.BLACK;
	public static final Color COLOR_OF_WALL = Color.GRAY;
}
