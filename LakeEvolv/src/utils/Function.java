package utils;

public class Function {

	public static int counterPlus(int i, int j) {
		if ((i + j) > 63) {
			return i + j - 64;
		} else {
			return i + j;
		}
	}

	public static int limitedPlus(int i, int j, int max) {
		if ((i + j) > max-1) {
			return max;
		} else {
			if ((i + j) < 0) {
				return 0;
			} else {
				return i + j;
			}
		}
	}
	
	public static boolean isRelative(int[] a, int[] b) {
		int t=0;
		for(int i = 0; i< a.length;i++) {
			if(a[i]!=b[i]) {
				t++;
				if(t==2) {
					return false;
				}
			}
		}
		return true;
	}
}
