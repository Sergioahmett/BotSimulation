package main;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import constants.MyConstants;
import gui.SimFrame;

public class MainSimClass implements MyConstants  {				
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new SimFrame(MyConstants.FIELD_WIDTH, MyConstants.FIELD_HIGH, "My Sim");		
			}
		});
	}

}
