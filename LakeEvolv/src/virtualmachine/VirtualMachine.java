package virtualmachine;

import constants.MyConstants;

import static utils.Function.*;

import java.util.Random;

import baseclasses.Bot;
import baseclasses.Cell;
import baseclasses.CellField;
import baseclasses.ListCreator;
import baseclasses.Organik;

public class VirtualMachine {
	private CellField cell;
	private Bot bot;
	private Organik org;
	private ListCreator<Bot> botList;
	private ListCreator<Organik> orgList;
	private int x;
	private int y;
	private boolean endMove;
	private int countOfOperation;
	private int counter;
	private int season;

	public VirtualMachine() {
	}

	public VirtualMachine(CellField cellsField, ListCreator<Bot> botArr, ListCreator<Organik> orgArr, String season) {
		this.cell = cellsField;
		this.botList = botArr;
		this.orgList = orgArr;
		this.season = chSeason(season);
	}

	private int chSeason(String season) {
		switch (season) {
		case "����":
			return 11;
		case "����":
			return 9;
		case "�����":
		case "�����":
			return 10;
		}
		return 0;
	}

	public void set(CellField cellsField, ListCreator<Bot> botArr, ListCreator<Organik> orgArr, String season) {
		this.cell = cellsField;
		this.botList = botArr;
		this.orgList = orgArr;
		this.season = chSeason(season);
	}

	public void go() {
		botMove();
		organikMove();
	}

	private void organikMove() {
		org = (Organik) orgList.getFirst();
		for (int i = 0; i < orgList.getCounterOfBots(); i++) {
			this.x = org.getX();
			this.y = org.getY();
			if (!org.isStop()) {
				if (cell.getCell(x, y + 1).getName() == "Empty") {
					cell.setCell(x, y + 1, new Cell("Organik", org));
					cell.setCell(x, y, new Cell("Empty"));
					org.setXY(x, y + 1);
				} else {
					org.setStop(true);
				}
			}
			org = (Organik) org.getNext();
		}
	}

	private void botMove() {
		bot = (Bot) botList.getFirst();
		for (int i = 0; i < botList.getCounterOfBots(); i++) {
			this.x = bot.getX();
			this.y = bot.getY();
			this.endMove = false;
			if (bot.getEnergy() > 0) {
				counter = bot.getTempBite();
				countOfOperation = 0;
				while (!endMove) {
					switch (bot.getBrain()[counter]) {
					case 23:	// relative spin
						spin();
						break;
					case 24:	// absolute spin
						spin();
						break;
					case 25:
						photosynthesis();
						break;
					case 26:	// relative move
						move(0);
						break;
					case 27:	// absolute move
						move(1);
						break;
					case 28:
						eat(0); // relative eat
						break;
					case 29:
						eat(1); // absolute eat
						break;
					case 30: 	// relative look
						look(0);
						break;
					case 31:	// absolute look
						look(1);
						break;
					case 32:
					case 42:
						shareResourses(0);	 //relative share
						break;
					case 33:
					case 51:
						shareResourses(1);	//absolute share
						break;
					case 34:
					case 50:
						transferEnergy(0);	//relative transfer
						break;
					case 35:
					case 52:
						transferEnergy(1);	//absolute transfer
						break;
					case 36:
						alignHorizontal();
						break;
					case 37:
						calculateMyLvl();
						break;
					case 38:
						calculateMyHP();
						break;
					case 39:
						calculateMyMineral();
						break;
					// case 40:
					// multicellular();
					// break;
					case 41:
						division(1);
						break;
					case 43:
						checkSurrounded();
						break;
					case 44:
						checkEnergyIncoming();
						break;
					case 45:
						checkMineralIncoming();
						break;
					// case 46:
					// checkMulticellular();
					// break;
					case 47:
						mineralToEnergy();
						break;
					case 48:
						mutate();
						break;
					// case 49:
					// genAtak();
					// break;
					default:
						goTo();
						break;
					}
				}
				bot.incMineral();
				checkBotLife();
			} else {
				DoORGANIK(bot);
			}

			bot = (Bot) bot.getNext();
		}

	}

	private void calculateMyMineral() {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			int p = (int) (bot.getBrain()[counterPlus(counter, 1)] * 15);
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, bot.getMinerals() < p ? 2 : 3)]);
			bot.setTempBite(counter);
			countOfOperation++;
		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}
	}

	private void calculateMyHP() {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			int p = (int) (bot.getBrain()[counterPlus(counter, 1)] * 15);
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, bot.getEnergy() < p ? 2 : 3)]);
			bot.setTempBite(counter);
			countOfOperation++;
		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}
	}

	private void transferEnergy(int i) {
		switch (bot.getDirection(i)) {
		case 0:
			transferCheck(-1, -1);
			break;
		case 1:
			transferCheck(-1, 0);
			break;
		case 2:
			transferCheck(-1, 1);
			break;
		case 7:
			transferCheck(0, -1);
			break;
		case 3:
			transferCheck(0, 1);
			break;
		case 6:
			transferCheck(1, -1);
			break;
		case 5:
			transferCheck(1, 0);
			break;
		case 4:
			transferCheck(1, 1);
			break;
		}

	}

	private void transferCheck(int k, int p) {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			switch (cell.getCell(x + p, y + k).getName()) {
			case "Empty":
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 2)]);
				break;
			case "Wall":
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 3)]);
				break;
			case "Organik":
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 4)]);
				break;
			case "Bot":
				Bot bot2 = cell.getCell(x + p, y + k).getBot();
				if (bot.getMinerals() > 4) {
					int minerals = bot.getMinerals() / 4;
					bot.setMinerals(bot.getMinerals() - minerals);
					bot2.setMinerals(limitedPlus(bot2.getMinerals(), minerals, 999));
				}
				int energ = bot.getEnergy() / 4;
				bot.setEnergy(bot.getEnergy() - energ);
				bot2.setEnergy(limitedPlus(bot2.getEnergy(), energ, 999));
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 5)]);
				break;
			}
			bot.setTempBite(counter);
			countOfOperation++;
		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}
	}
	// TODO
	// private void genAtak() {
	// }

	private void mutate() {
		bot.mutate();
		counter = counterPlus(counter, 1);
		bot.setTempBite(counter);
		endMove = true;
	}

	private void mineralToEnergy() {
		bot.setEnergy(bot.getEnergy() - 3);
		if (bot.getMinerals() > 200) {
			bot.setMinerals(bot.getMinerals() - 100);
			bot.setEnergy(bot.getEnergy() + 400);
		} else {
			bot.setEnergy(bot.getEnergy() + bot.getMinerals() * 4);
			bot.setMinerals(0);
		}
		bot.doBlue();
		counter = counterPlus(counter, 1);
		bot.setTempBite(counter);
		endMove = true;
	}

	// TODO
	// private void checkMulticellular() {
	// }

	private void checkMineralIncoming() {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter,
					bot.getY() > MyConstants.UPPER_MINERAL_INCOMING_BORDER ? 1 : 2)]);
			bot.setTempBite(counter);
			countOfOperation++;
		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}

	}

	private void checkEnergyIncoming() {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			int temp = 0;
			if (bot.getMinerals() < 100) {
				temp = 0;
			} else {
				if (bot.getMinerals() < 400) {
					temp = 1;
				} else {
					temp = 2;
				}
			}
			temp = season - ((bot.getY() - 1) / 7) + temp;
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, temp >= 3 ? 1 : 2)]);
			bot.setTempBite(counter);
			countOfOperation++;
		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}
	}

	private void checkSurrounded() {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, chackSurround() == null ? 1 : 2)]);
			bot.setTempBite(counter);
			countOfOperation++;
		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}
	}

	private void division(int i) {
		Random r = new Random();
		bot.setEnergy(bot.getEnergy() - MyConstants.ENERGY_TO_DIVISION);
		if (bot.getEnergy() < 0) {
			DoORGANIK(bot);
		} else {
			int[][] temp = chackSurround();
			if (temp == null) {
				DoORGANIK(bot);
			} else {
				int k = r.nextInt(temp.length);
				botList.addElement(new Bot(temp[k][1], temp[k][0], bot));
				bot.setEnergy(bot.getEnergy() / 2);
				bot.setMinerals(bot.getMinerals() / 2);
			}
		}
		if (i == 1) {
			counter = counterPlus(counter, 1);
			bot.setTempBite(counter);
		}
		endMove = true;
	}

	private int[][] chackSurround() {
		int c = 0;
		for (int i = bot.getY() - 1; i <= bot.getY() + 1; i++) {
			for (int j = bot.getX() - 1; j <= bot.getX() + 1; j++) {
				if (cell.getCell(j, i).getName() == "Empty") {
					c++;
				}
			}
		}
		if (c == 0) {
			return null;
		}
		int[][] temp = new int[c][];
		c = 0;
		for (int i = bot.getY() - 1; i <= bot.getY() + 1; i++) {
			for (int j = bot.getX() - 1; j <= bot.getX() + 1; j++) {
				if (cell.getCell(j, i).getName() == "Empty") {
					temp[c++] = new int[] { i, j };
				}
			}
		}

		return temp;
	}
	// TODO
	// private void multicellular() {
	// }

	private void calculateMyLvl() {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			int p = (int) (bot.getBrain()[counterPlus(counter, 1)] * 1.77);
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, bot.getY() < p ? 2 : 3)]);
			bot.setTempBite(counter);
			countOfOperation++;
		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}

	}

	private void alignHorizontal() {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			Random r = new Random();
			bot.setView(r.nextBoolean() ? 3 : 7);
			counter = counterPlus(counter, 1);
			bot.setTempBite(counter);
			countOfOperation++;
		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}
	}

	private void shareResourses(int i) {
		switch (bot.getDirection(i)) {
		case 0:
			shareChek(-1, -1);
			break;
		case 1:
			shareChek(-1, 0);
			break;
		case 2:
			shareChek(-1, 1);
			break;
		case 7:
			shareChek(0, -1);
			break;
		case 3:
			shareChek(0, 1);
			break;
		case 6:
			shareChek(1, -1);
			break;
		case 5:
			shareChek(1, 0);
			break;
		case 4:
			shareChek(1, 1);
			break;
		}
	}

	private void shareChek(int k, int p) {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			switch (cell.getCell(x + p, y + k).getName()) {
			case "Empty":
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 2)]);
				break;
			case "Wall":
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 3)]);
				break;
			case "Organik":
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 4)]);
				break;
			case "Bot":
				Bot bot2 = cell.getCell(x + p, y + k).getBot();
				if (bot.getMinerals() > bot2.getMinerals()) {
					int minerals = (bot.getMinerals() + bot2.getMinerals()) / 2;
					bot.setMinerals(minerals);
					bot2.setMinerals(minerals);
				}
				if (bot.getEnergy() > bot2.getEnergy()) {
					int energ = (bot.getEnergy() + bot2.getEnergy()) / 2;
					bot.setEnergy(energ);
					bot2.setEnergy(energ);
				}
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 5)]);
				break;
			}
			bot.setTempBite(counter);
			countOfOperation++;
		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}
	}

	private void look(int i) {
		switch (bot.getDirection(i)) {
		case 0:
			lookChek(-1, -1);
			break;
		case 1:
			lookChek(-1, 0);
			break;
		case 2:
			lookChek(-1, 1);
			break;
		case 7:
			lookChek(0, -1);
			break;
		case 3:
			lookChek(0, 1);
			break;
		case 6:
			lookChek(1, -1);
			break;
		case 5:
			lookChek(1, 0);
			break;
		case 4:
			lookChek(1, 1);
			break;
		}
	}

	private void lookChek(int k, int p) {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			switch (cell.getCell(x + p, y + k).getName()) {
			case "Empty":
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 2)]);
				break;
			case "Bot":
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter,
						isRelative(bot.getBrain(), cell.getCell(x + p, y + k).getBot().getBrain()) ? 5 : 6)]);
				break;
			case "Wall":
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 3)]);
				break;
			case "Organik":
				counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 4)]);
				break;
			}
			bot.setTempBite(counter);
			countOfOperation++;
		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}
	}

	private void eat(int i) {
		switch (bot.getDirection(i)) {
		case 0:
			eatChek(-1, -1);
			break;
		case 1:
			eatChek(-1, 0);
			break;
		case 2:
			eatChek(-1, 1);
			break;
		case 7:
			eatChek(0, -1);
			break;
		case 3:
			eatChek(0, 1);
			break;
		case 6:
			eatChek(1, -1);
			break;
		case 5:
			eatChek(1, 0);
			break;
		case 4:
			eatChek(1, 1);
			break;
		}
		endMove = true;

	}

	private void eatChek(int k, int p) {
		bot.setEnergy(bot.getEnergy() - 4);
		switch (cell.getCell(x + p, y + k).getName()) {
		case "Empty":
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 3)]);
			break;
		case "Wall":
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 2)]);
			break;
		case "Organik":
			orgList.dellElement(cell.getCell(x + p, y + k).getOrg());
			cell.setCell(x + p, y + k, new Cell("Empty"));
			bot.setEnergy(bot.getEnergy() + 100);
			bot.doRed();
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 4)]);
			break;
		case "Bot":
			fight(bot, cell.getCell(x + p, y + k).getBot());
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 5)]);
			break;
		}
		bot.setTempBite(counter);

	}

	private void fight(Bot bot1, Bot bot2) {
		if (bot1.getMinerals() >= bot2.getMinerals()) {
			bot1.setMinerals(bot1.getMinerals() - bot2.getMinerals());
			// if (bot1.getMinerals() < bot2.getEnergy() / 2) {
			// bot1.setMinerals(0);
			// } else {
			// bot1.setMinerals(bot1.getMinerals() - bot2.getEnergy() / 2);
			// }
			bot1.plusEnergy(100 + (bot2.getEnergy() / 2));
			bot1.doRed();
			cell.setCell(bot2.getX(), bot2.getY(), new Cell("Empty"));
			botList.dellElement(bot2);
		} else {
			bot2.setMinerals(bot2.getMinerals() - bot1.getMinerals());
			bot1.setMinerals(0);
			if (bot1.getEnergy() > bot2.getMinerals() * 2) {
				bot1.plusEnergy(100 + (bot2.getEnergy() / 2) - (2 * bot2.getMinerals()));
				bot1.doRed();
				cell.setCell(bot2.getX(), bot2.getY(), new Cell("Empty"));
				botList.dellElement(bot2);
			} else {
				DoORGANIK(bot1);
			}
		}
	}

	private void move(int i) {
		switch (bot.getDirection(i)) {
		case 0:
			moveChek(-1, -1);
			break;
		case 1:
			moveChek(-1, 0);
			break;
		case 2:
			moveChek(-1, 1);
			break;
		case 7:
			moveChek(0, -1);
			break;
		case 3:
			moveChek(0, 1);
			break;
		case 6:
			moveChek(1, -1);
			break;
		case 5:
			moveChek(1, 0);
			break;
		case 4:
			moveChek(1, 1);
			break;
		}
		endMove = true;
	}

	private void moveChek(int k, int p) {
		bot.setEnergy(bot.getEnergy() - 3);
		switch (cell.getCell(x + p, y + k).getName()) {
		case "Empty":
			moveTo(x + p, y + k);
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 2)]);
			break;
		case "Bot":
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter,
					isRelative(bot.getBrain(), cell.getCell(x + p, y + k).getBot().getBrain()) ? 5 : 6)]);
			break;
		case "Wall":
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 3)]);
			break;
		case "Organik":
			counter = counterPlus(counter, bot.getBrain()[counterPlus(counter, 4)]);
			break;
		}
		bot.setTempBite(counter);
	}

	private void photosynthesis() {
		int temp = 0;
		if (bot.getMinerals() < 100) {
			temp = 0;
		} else {
			if (bot.getMinerals() < 400) {
				temp = 1;
			} else {
				temp = 2;
			}
		}
		temp = season - ((bot.getY() - 1) / 7) + temp;
		bot.plusEnergy(temp - 3);
		if (temp > 0) {
			bot.doGreen();
		} else {
			bot.doLessGreen();
		}
		counter = counterPlus(counter, 1);
		bot.setTempBite(counter);
		endMove = true;
	}

	private void checkBotLife() {
		if (bot.getEnergy() > MyConstants.MAX_BOT_ENERGY) {
			division(0);
		}
	}

	private void moveTo(int x2, int y2) {
		cell.setCell(x2, y2, new Cell("Bot", bot));
		cell.setCell(x, y, new Cell("Empty"));
		bot.setXY(x2, y2);
	}

	private void DoORGANIK(Bot bot) {
		orgList.addElement(new Organik(bot.getX(), bot.getY()));
		botList.dellElement(bot);
	}

	private void goTo() {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			counter = counterPlus(counter, bot.getBrain()[counter]);
			bot.setTempBite(counter);
			countOfOperation++;

		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}
	}

	private void spin() {
		if (countOfOperation < MyConstants.MAX_COUNT_OF_OPERATION_BY_ONE_MOVE) {
			int k = 0;
			if (bot.getBrain()[counter] == 23) {
				k = bot.getView() + bot.getBrain()[counterPlus(counter, 1)] / 8;
				if (k > 7) {
					k = k - 8;
				}
			} else {
				k = bot.getBrain()[counterPlus(counter, 1)] / 8;
			}
			bot.setView(k);
			counter = counterPlus(counter, 2);
			bot.setTempBite(counter);
			countOfOperation++;
		} else {
			bot.setEnergy(bot.getEnergy() - 3);
			endMove = true;
		}

	}

}
