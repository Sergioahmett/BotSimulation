package mainlogic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.JPanel;

import baseclasses.Bot;
import baseclasses.Organik;
import constants.MyConstants;

public class SimPanel extends JPanel implements Runnable { 
															

	private static final long serialVersionUID = 1L;
	private Thread simThread = null; 
	private Thread curentThread = null; 
	private SimProcessor life = null;
	private int updateDelay = 1000;
	private int typeOfPainting = 0;

	public SimPanel() {
		setBackground(Color.WHITE);
	}

	public SimProcessor getLifeModel() {
		return life;
	}

	public void initialize(int width, int height) { 
		life = new SimProcessor(width, height);
	}

	public void startSimulation() {
		if (simThread == null) {
			simThread = new Thread(this);
			curentThread = simThread;
			simThread.start();
		}
	}

	public void stopSimulation() {
		simThread = null;

	}

	public boolean isSimulating() {
		return simThread != null;
	}

	public Thread getCurentThread() { 
		return curentThread;
	}

	public void run() {
		repaint();
		while (simThread != null) {
			try {
				Thread.sleep(updateDelay);
			} catch (InterruptedException e) {
			}
			life.simulate();
			repaint();
		}
	}

	public Dimension getPreferredSize() { 
		if (life != null) {
			Insets b = getInsets();
			return new Dimension(
					(MyConstants.CELL_SIZE + MyConstants.CELL_GAP) * life.getWidth() + MyConstants.CELL_GAP + b.left
							+ b.right,
					(MyConstants.CELL_SIZE + MyConstants.CELL_GAP) * (life.getHigh()) + MyConstants.CELL_GAP + b.top
							+ b.bottom);   
		} else
			return new Dimension(100, 100);
	}

	protected void paintComponent(Graphics g) { 
		if (life != null) {
			synchronized (life) {
				super.paintComponent(g);
				paintBG(g);
				paintBots(g);
				paintOrgs(g);
				}
		}
	}

	public void paintBots(Graphics g) {
		Insets b = getInsets();
		Bot temp =(Bot) life.getMyBots().getFirst();
		for (int y = 0; y < life.getMyBots().getCounterOfBots(); y++) {
			g.setColor(getColor(temp));

			g.fillRect(b.left + MyConstants.CELL_GAP + temp.getX() * (MyConstants.CELL_SIZE + MyConstants.CELL_GAP),
					b.top + MyConstants.CELL_GAP + temp.getY() * (MyConstants.CELL_SIZE + MyConstants.CELL_GAP),
					MyConstants.CELL_SIZE, MyConstants.CELL_SIZE);
			temp = (Bot)temp.getNext();

		}
	}

	private Color getColor(Bot bot) {
		if (typeOfPainting == 0) {
			return bot.getColor();
		}
		if (typeOfPainting == 1) {
			return bot.getEnColor();
		}
		if (typeOfPainting == 2) {
			return bot.getMinColor();
		}
		return null;
	}

	public int getTypeOfPainting() {
		return typeOfPainting;
	}

	public void setTypeOfPainting(int typeOfPainting) {
		this.typeOfPainting = typeOfPainting;
	}

	public void paintOrgs(Graphics g) {
		Insets b = getInsets();
		Organik temp =(Organik) life.getMyOrgs().getFirst();
		for (int y = 0; y < life.getMyOrgs().getCounterOfBots(); y++) {
			g.setColor(Organik.getColor());
			g.fillRect(b.left + MyConstants.CELL_GAP + temp.getX() * (MyConstants.CELL_SIZE + MyConstants.CELL_GAP),
					b.top + MyConstants.CELL_GAP + temp.getY() * (MyConstants.CELL_SIZE + MyConstants.CELL_GAP),
					MyConstants.CELL_SIZE, MyConstants.CELL_SIZE);
			temp =(Organik) temp.getNext();

		}
	}

	public void paintBG(Graphics g) {
		for (int y = 0; y < 16; y++) {
			g.setColor(getMyColor(y));
			g.fillRect(0, y * 7 * (MyConstants.CELL_SIZE + MyConstants.CELL_GAP),
					200 * (MyConstants.CELL_SIZE + MyConstants.CELL_GAP),
					112 * (MyConstants.CELL_SIZE + MyConstants.CELL_GAP));
		}
	}

	public static Color getMyColor(int y) {
		switch (y) {
		case 0:
			return new Color(255, 255, 255);
		case 1:
			return new Color(250, 250, 255);
		case 2:
			return new Color(245, 245, 255);
		case 3:
			return new Color(240, 240, 255);
		case 4:
			return new Color(235, 235, 255);
		case 5:
			return new Color(230, 230, 255);
		case 6:
			return new Color(225, 225, 255);
		case 7:
			return new Color(220, 220, 255);
		case 8:
			return new Color(215, 215, 255);
		case 9:
			return new Color(210, 210, 255);
		case 10:
			return new Color(205, 205, 255);
		case 11:
			return new Color(200, 200, 255);
		case 12:
			return new Color(195, 195, 255);
		case 13:
			return new Color(190, 190, 255);
		case 14:
			return new Color(185, 185, 255);
		case 15:
			return new Color(180, 180, 255);
		}

		return Color.WHITE;
	}

	public void setUpdateDelay(int updateDelay) {
		this.updateDelay = updateDelay;

	}
}
