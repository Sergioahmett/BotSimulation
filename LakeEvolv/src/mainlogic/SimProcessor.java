package mainlogic;

import baseclasses.Bot;
import baseclasses.Cell;
import baseclasses.CellField;
import baseclasses.ListCreator;
import baseclasses.Organik;
import virtualmachine.VirtualMachine;

public class SimProcessor {
	private CellField cellsField;
	private ListCreator<Bot> myBots;
	private ListCreator<Organik> myOrgs;
	private int seasonTimer = 0;
	private String season = "����";
	private VirtualMachine VM;
	
	private int width;
	private int high;

	public SimProcessor(int width, int high) {
		this.width = width;
		this.high = high;
		cellsField = new CellField(width, high);
		myBots = new ListCreator<>(cellsField);
		myOrgs = new ListCreator<>(cellsField);
		addBots();
		VM = new VirtualMachine();

	}

	public void simulate() {
		VM.set(cellsField, myBots, myOrgs, season);
		VM.go();

		if (seasonTimer++ > 10000) {
			chSeason();
			seasonTimer = 0;
		}
	}

	public void addBots() {
		myBots.addElement(new Bot(100, 25));
	}

	public ListCreator<Bot> getMyBots() {
		return myBots;
	}

	public ListCreator<Organik> getMyOrgs() {
		return myOrgs;
	}

	public Cell getCells(int i, int j) {
		return cellsField.getCell(j, i);
	}

	public int getWidth() {
		return width;
	}

	public int getHigh() {
		return high;
	}

	private void chSeason() {
		switch(season) {
		case "����":
			season = "�����";
			break;
		case "�����":
			season = "����";
			break;
		case "����":
			season = "�����";
			break;
		case "�����":
			season = "����";
			break;
		}
	}
}
