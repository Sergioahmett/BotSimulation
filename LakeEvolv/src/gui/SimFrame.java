package gui;

import java.awt.BorderLayout;
import javax.swing.*;

import mainlogic.SimPanel;

public class SimFrame extends JFrame { 

	private static final long serialVersionUID = 1L;
	private SimPanel lifePanel = null;
	private JButton startButton = null;
	private JSlider simulationSpeedSlider = null;
	private JToolBar upperToolBar = null;

	public SimFrame(int i, int j, String title) {
		super(title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		lifePanel = new SimPanel();
		lifePanel.initialize(i, j); 
		add(lifePanel);
		setSize(lifePanel.getPreferredSize());
		addUpperToolBar();
		pack();
		setVisible(true);
	}

	private void addUpperToolBar() {
		upperToolBar = new JToolBar();
		upperToolBar.setFloatable(false);
		add(upperToolBar, BorderLayout.NORTH);
		addStartButton();
		addSpeedSlider();
		addDisplaingType();
	}

	private void addDisplaingType() {
		ButtonGroup group = new ButtonGroup();
		JRadioButton typeFlag = new JRadioButton("Type", true);
		group.add(typeFlag);
		JRadioButton energyFlag = new JRadioButton("Energy", false);
		group.add(energyFlag);
		JRadioButton mineralFlag = new JRadioButton("Mineral", false);
		group.add(mineralFlag);
		typeFlag.addActionListener(e->lifePanel.setTypeOfPainting(0));
		energyFlag.addActionListener(e->lifePanel.setTypeOfPainting(1));
		mineralFlag.addActionListener(e->lifePanel.setTypeOfPainting(2));
		upperToolBar.add(typeFlag);
		upperToolBar.add(energyFlag);
		upperToolBar.add(mineralFlag);
	}

	private void addSpeedSlider() {
		simulationSpeedSlider = new JSlider(0, 100);
		simulationSpeedSlider.setValue(10);
		lifePanel.setUpdateDelay(10);
		simulationSpeedSlider.addChangeListener(e -> lifePanel.setUpdateDelay(simulationSpeedSlider.getValue()));
		upperToolBar.addSeparator();
		upperToolBar.add(new JLabel("Faster"));
		upperToolBar.add(simulationSpeedSlider);
		upperToolBar.add(new JLabel("Slower"));
	}

	private void addStartButton() {
		startButton = new JButton("Start");
		upperToolBar.add(startButton);
		startButton.addActionListener(e -> {
			if (lifePanel.isSimulating()) {
				lifePanel.stopSimulation();
				startButton.setText("Start");
			} else {
				lifePanel.startSimulation();
				startButton.setText("Stop");
			}
		});
	}
}
