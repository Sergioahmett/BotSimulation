package baseclasses;

import static utils.Function.*;

import java.awt.Color;
import java.util.Random;

import constants.MyConstants;
import enums.BotType;

public class Bot extends BaseEntity {
	private Color color;
	private int[] brain;
	private int tempBite;
	private int view; 
	private int energy;
	private BotType type;
	private int minerals = 0;

	public Bot(int x, int y) {
		super(x, y);
		this.brain = new int[64];
		Random r = new Random();
		init();
		this.tempBite = 0;
		this.view = r.nextInt(8);
		this.energy = MyConstants.BOT_START_ENERGY;
		this.type = BotType.ALIVE_BOT;
		this.color = Color.RED;
	}

	public Bot(int x, int y, Bot bot) {
		setX(x);
		this.y = y;
		Random r = new Random();
		this.brain = brainTransferByChance(bot.getBrain());
		this.tempBite = 0;
		this.view = r.nextInt(8);
		this.energy = bot.getEnergy() / 2;
		this.minerals = bot.getMinerals() / 2;
		this.type = BotType.ALIVE_BOT;
		this.color = bot.getColor();
	}


	public void mutate() {
		Random r = new Random();
		brain[r.nextInt(64)] = r.nextInt(64);
		brain[r.nextInt(64)] = r.nextInt(64);
	}

	public int getDirection(int i) {
		int k = 0;
		if (i == 0) {
			k = this.view + this.brain[counterPlus(tempBite, 1)] / 8;
			if (k > 7) {
				k = k - 8;
			}
		} else {
			k = this.brain[counterPlus(tempBite, 1)] / 8;
		}
		return k;
	}

	public Color getColor() {
		return this.color;
	}

	public Color getEnColor() {
		return new Color(255, limitedPlus(255, -(getEnergy() / 4), 255), limitedPlus(255, -getEnergy() / 4, 255));
	}

	public Color getMinColor() {
		return new Color(limitedPlus(255, (int) -Math.sqrt(getMinerals() * 65), 255),
				limitedPlus(255, (int) -Math.sqrt(getMinerals() * 65), 255), 255);
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int[] getBrain() {
		return brain;
	}

	public void setBrain(int[] brain) {
		this.brain = brain;
	}

	public int getView() {
		return view;
	}

	public void setView(int view) {
		this.view = view;
	}

	public int getTempBite() {
		return tempBite;
	}

	public void setTempBite(int tempBite) {
		this.tempBite = tempBite;
	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int energy) {
		this.energy = energy;
	}

	public void plusEnergy(int plus) {
		this.energy = this.energy + plus;
	}

	@Override
	public void setX(int x) {
		if (x > 199) {
			this.x = 0;
		} else {
			if (x < 0) {
				this.x = 199;
			} else {
				this.x = x;
			}
		}
	}

	public void setXY(int x, int y) {
		setX(x);
		this.y = y;
	}

	public BotType getType() {
		return type;
	}

	public void setType(BotType type) {
		this.type = type;
	}

	public int getMinerals() {
		return minerals;
	}

	public void setMinerals(int minerals) {
		this.minerals = minerals;
	}

	public void plusMinerals(int plus) {
		this.minerals = this.minerals + plus;
	}

	public void doGreen() {
		if ((this.color.getGreen() + 100) > 255) {
			this.color = new Color((this.color.getRed() / 3), 255, (this.color.getBlue() / 3));
		} else {
			this.color = new Color((this.color.getRed() / 3), (this.color.getGreen() + 100),
					(this.color.getBlue() / 3));
		}
	}

	public void doLessGreen() {
		if ((this.color.getGreen() + 10) > 255) {
			this.color = new Color((this.color.getRed() / 2), 255, (this.color.getBlue() / 2));
		} else {
			this.color = new Color((this.color.getRed() / 2), (this.color.getGreen() + 10), (this.color.getBlue() / 2));
		}
	}

	public void doBlue() {
		if ((this.color.getBlue() + 100) > 255) {
			this.color = new Color((this.color.getRed() / 3), (this.color.getGreen() / 3), 255);
		} else {
			this.color = new Color((this.color.getRed() / 3), (this.color.getGreen() / 3),
					(this.color.getBlue() + 100));
		}
	}

	public void doRed() {
		if ((this.color.getRed() + 100) > 255) {
			this.color = new Color(255, (this.color.getGreen() / 3), (this.color.getBlue() / 3));
		} else {
			this.color = new Color((this.color.getRed() + 100), (this.color.getGreen() / 3),
					(this.color.getBlue() / 3));
		}
	}

	public void incMineral() {
		if (((getY() - 1) / 7 > 7) && ((getY() - 1) / 7 < 11)) {
			setMinerals(limitedPlus(getMinerals(), 1, 1000));
		}
		if (((getY() - 1) / 7 > 10) && ((getY() - 1) / 7 < 14)) {
			setMinerals(limitedPlus(getMinerals(), 2, 1000));
		}
		if ((getY() - 1) / 7 > 13) {
			setMinerals(limitedPlus(getMinerals(), 3, 1000));
		}
	}
	
	private void init() {
		for (int i = 0; i < 64; i++) {
			brain[i] = 25;
		}
	}
	
	private int[] brainTransferByChance(int[] brain2) {
		Random r = new Random();
		if (r.nextInt(4) == 0) {
			int[] temp = new int[64];
			for (int i = 0; i < 64; i++) {
				temp[i] = brain2[i];
			}
			temp[r.nextInt(64)] = r.nextInt(64);
			return temp;
		} else {
			return brain2;
		}
	}
}
