package baseclasses;

public class ListCreator<T extends BaseEntity> {

	private int counterOfBots = 0;
	private BaseEntity first;
	private BaseEntity last;
	private CellField cell;

	public ListCreator(CellField cellsField) {
		this.cell = cellsField;
	}

	public void addElement(T bot) {
		if (counterOfBots == 0) {
			bot.setPrev(bot);
			this.first = bot;
			this.last = bot;
		} else {
			last.setNext(bot);
			bot.setPrev(last);
			last = bot;
		}
		counterOfBots++;
		paint(bot);
	}

	public void paint(BaseEntity bot) { 
		if(bot instanceof Bot) {
			cell.setCell(bot.getX(), bot.getY(), new Cell("Bot", bot));	
		}else {
			cell.setCell(bot.getX(), bot.getY(), new Cell("Organik", bot));	
		}
	}

	public int getCounterOfBots() {
		return counterOfBots;
	}

	@SuppressWarnings("unchecked")
	public T getElement(int k) {
		if (k >= counterOfBots) {
			throw new IndexOutOfBoundsException();
		}
		BaseEntity temp = this.first;
		for (int i = 0; i < k; i++) {
			temp = temp.getNext();
		}
		return (T) temp;
	}

	public void dellFirst() {
		this.first = this.getFirst().getNext();
		this.first.setPrev(this.first);
		counterOfBots--;
	}

	public void dellLast() {
		this.last = this.last.getPrev();
		this.last.setNext(this.last);
		counterOfBots--;

	}

	public void dellElement(BaseEntity dellBot) {
		if (dellBot == first) {
			dellFirst();
		} else {
			if (dellBot == last) {
				dellLast();
			} else {
				dellBot.getPrev().setNext(dellBot.getNext());
				dellBot.getNext().setPrev(dellBot.getPrev());
				counterOfBots--;
			}
		}

	}

	public void dellElement(int k) {
		if (k >= counterOfBots) {
			throw new IndexOutOfBoundsException();
		}
		BaseEntity temp = this.first;
		for (int i = 0; i < k; i++) {
			temp = temp.getNext();
		}
		dellElement(temp);

	}

	public void setCounterOfBots(int counterOfBots) {
		this.counterOfBots = counterOfBots;
	}

	public BaseEntity getFirst() {
		return first;
	}

	public void setFirst(BaseEntity first) {
		this.first = first;
	}

}
