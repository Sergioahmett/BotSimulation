package baseclasses;

import java.awt.Color;

public class Organik extends BaseEntity {
	private static final Color color = Color.GRAY;
	private boolean isStop=false;

	public Organik(int x, int y) {
		super(x,y);
	}


	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public static Color getColor() {
		return color;
	}

	public String toStringXY() {
		return "x= " + getX() + " y=" + getY();
	}

	public boolean isStop() {
		return isStop;
	}

	public void setStop(boolean isStop) {
		this.isStop = isStop;
	}

}
