package baseclasses;

public class BaseEntity {
	protected int x;
	protected int y;
	protected BaseEntity next = this;
	protected BaseEntity prev = this;

	protected BaseEntity() {
	}

	protected BaseEntity(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public BaseEntity getNext() {
		return next;
	}

	public void setNext(BaseEntity next) {
		this.next = next;
	}

	public BaseEntity getPrev() {
		return prev;
	}

	public void setPrev(BaseEntity prev) {
		this.prev = prev;
	}
}
