package baseclasses;

public class Cell {
	private String name;
	private Bot bot;
	private Organik org;

	public Cell(String name) {
		this.setName(name);
		bot = null;
		org = null;
	}

	public Cell(String name, BaseEntity bot) {
		this.setName(name);
		if (bot instanceof Organik) {
			this.setOrg((Organik)bot);
			bot = null;
		}
		if (bot instanceof Bot) {
			this.setBot((Bot)bot);
			org = null;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Bot getBot() {
		return bot;
	}

	public void setBot(Bot bot) {
		this.bot = bot;
	}

	public Organik getOrg() {
		return org;
	}

	public void setOrg(Organik org) {
		this.org = org;
	}
}
