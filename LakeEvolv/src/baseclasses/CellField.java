package baseclasses;

public class CellField {
	private Cell[][] cells;

	public CellField(int w, int h) {
		cells = newEmptyField(w,h);
	}
	
	public void setCell(int x, int y, Cell cell) {
		cells[y][checkX(x)] = cell;
	}
	
	public Cell getCell(int x, int y) {
		return cells[y][checkX(x)];
	}

	private int checkX(int x) {
		if(x>199) {
			return 0;
		}
		if(x<0) {
			return 199;
		}
		return x;
	}
	
	private Cell[][] newEmptyField(int w, int h) {
		Cell[][] cellsField = new Cell[h][w];
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				cellsField[i][j] = new Cell("Empty");
			}
		}
		for (int i = 0; i<w;i++) {
			cellsField[0][i] = new Cell("Wall");
			cellsField[h-1][i] = new Cell("Wall");
		}
		return cellsField;
	}
}
