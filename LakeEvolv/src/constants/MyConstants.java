package constants;

public interface MyConstants {				
	public static final String FRAME_NAME = "Simulation of Life";		
	public static final int FIELD_HIGH = 114;							
	public static final int FIELD_WIDTH = 200;
	public static final int BOT_START_ENERGY = 60;								
	public static final int MAX_BOT_ENERGY = 999;							
	public static final int CELL_SIZE = 5;								
	public static final int CELL_GAP = 1;	
	public static final int SEASON_TIME = 10000;
	public static final int ENERGY_TO_DIVISION = 150;
	public static final int UPPER_MINERAL_INCOMING_BORDER  = 150;
	public static final int MAX_COUNT_OF_OPERATION_BY_ONE_MOVE  = 10;
}
