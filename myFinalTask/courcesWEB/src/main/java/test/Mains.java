package test;

import java.util.LinkedList;
import java.util.List;

import com.epam.daoLayer.daoEntity.DatabaseUser;
import com.epam.daoLayer.dbFasad.DBFasad;
import com.epam.interfaces.DatabaseFasadInterface;

import utils.Journal;

public class Mains {
    public static void main(String[] args) {
//        Journal j = new Journal();
//        List<DatabaseUser> users = new LinkedList<>();
//        for (int i = 0; i < 10; i++) {
//            users.add(DatabaseUser.newBuilder().setId(i).setName("aaaa" + i).setSurname("bbbb" + i).build());
//        }
//        j.addStudents(users);
//        j.addDayToJournal("11.11");
//        j.addDayToJournal("11.12");
//        j.addDayToJournal("11.13");
//        j.addDayToJournal("11.14");
//        j.addRaitingToUserByDay(5, "11.13", "5");
//        j.addDayToJournal("22.11");
//        j.output();
//        System.out.println(j.getJSON());
        
        DatabaseFasadInterface dbf = DBFasad.getInstance();
        System.out.println(dbf.checkAccessToJournal(11, 10, "Teacher"));
        dbf.getJournal(10, "Teacher").output();
    }

}
