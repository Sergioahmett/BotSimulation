package com.epam.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.annotation.ProcessorAnnotationController;
import com.epam.interfaces.ProcessorIntarface;
import com.epam.processor.*;

/**
 * Main servlet controller.
 * 
 * @author D.Kolesnikov
 * 
 */
@WebServlet(name = "headServlet", urlPatterns = { "/teachers/*", "/courses/*", "/profile/*", "/auth/*", "/theams/*",
        "/admin/*", "/journal/*", "/register/*" })
public class Controller extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private List<ProcessorIntarface> processorList = new LinkedList<>();

    public void init() throws ServletException {
        processorList.add(AdminProcessor.getInstance());
        processorList.add( JournalProcessor.getInstance());
        processorList.add( CoursesProcessor.getInstance());
        processorList.add( TheamsProcessor.getInstance());
        processorList.add( AuthProcessor.getInstance());
        processorList.add( UserProcessor.getInstance());
        processorList.add( TeacherProcessor.getInstance());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        redirect(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        redirect(request, response);
    }

    /**
     * Main method of this controller.
     */
    private void redirect(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        ProcessorIntarface currentProcessor = null;
        System.out.println(request.getRequestURI());
        ProcessorAnnotationController annotationController = new ProcessorAnnotationController();
        for (ProcessorIntarface processor : processorList) {
            if (annotationController.checkPath(processor.getClass(), request.getRequestURI())) {
                currentProcessor = processor;
                break;
            }
        }
        if (currentProcessor != null) {
            currentProcessor.startProcess(request, response);
        } else {
            // processorMap.get("^/error.*").startProcess(request, response);
        }
    }

    public static void main(String[] args) {
    }
}