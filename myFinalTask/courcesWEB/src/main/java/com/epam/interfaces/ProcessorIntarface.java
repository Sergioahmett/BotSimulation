package com.epam.interfaces;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ProcessorIntarface {
    public void startProcess(HttpServletRequest request, HttpServletResponse response);
}
