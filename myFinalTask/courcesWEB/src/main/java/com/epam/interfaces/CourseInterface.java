package com.epam.interfaces;

import java.sql.Timestamp;

public interface CourseInterface {
public Timestamp getStartDate();
public int getDuration();
}
