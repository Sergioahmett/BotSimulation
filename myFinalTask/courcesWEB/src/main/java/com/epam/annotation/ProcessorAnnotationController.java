package com.epam.annotation;

import java.util.regex.Pattern;

public class ProcessorAnnotationController {
    public boolean checkPath(Class<?> clazz, String webPath){
        if (clazz.isAnnotationPresent(Processor.class)) {
            Processor proc = clazz.getAnnotation(Processor.class);
            String path = proc.path();
            if (path != null && !path.isEmpty()) {
                return Pattern.compile("^" + path + ".*").matcher(webPath).find();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
