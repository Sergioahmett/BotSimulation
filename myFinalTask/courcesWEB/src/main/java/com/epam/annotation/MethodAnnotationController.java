package com.epam.annotation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.annotation.HTTPMethod;

public class MethodAnnotationController {

    public void redirectToMethod(Object obj, HttpServletRequest request, HttpServletResponse response) {
        if (obj.getClass().isAnnotationPresent(Processor.class)) {
            Method[] methods = obj.getClass().getMethods();
            Method targetMethod = null;
            for (Method method : methods) {
                if (method.isAnnotationPresent(HTTPMethod.class) && method.isAnnotationPresent(RequestMappin.class)) {
                    String methodType = method.getAnnotation(HTTPMethod.class).method().toString();
                    if (request.getMethod().equals(methodType)) {
                        if (checkMethod(obj.getClass().getAnnotation(Processor.class),
                                method.getAnnotation(RequestMappin.class), request)) {
                            targetMethod = method;
                        }
                    }
                }
            }
            if (targetMethod != null) {
                try {
                    targetMethod.invoke(obj, request, response);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Method not found");
            }
        } else {
            System.out.println("Not Processor");
        }
    }

    private boolean checkMethod(Processor procAnnotation, RequestMappin requestAnnotation, HttpServletRequest request) {
        if (checkPath(procAnnotation.path(), requestAnnotation.path(), request.getRequestURI(),
                !(requestAnnotation.pathVariable().length()==0))) {
            if (requestAnnotation.requiredParam().length != 0) {
                if (checkParam(request.getParameterMap(), requestAnnotation.requiredParam(),
                        requestAnnotation.requiredParamVariable())) {
                    setPathVariable(procAnnotation.path(), requestAnnotation.path(), requestAnnotation.pathVariable(),
                            request);
                    return true;
                }
            } else {
                setPathVariable(procAnnotation.path(), requestAnnotation.path(), requestAnnotation.pathVariable(),
                        request);
                return true;
            }
        }
        return false;
    }

    private void setPathVariable(String procPath, String methodPath, String pathVariable, HttpServletRequest request) {
        if (!pathVariable.isEmpty()) {
            String variable = request.getRequestURI().replace(procPath + methodPath.replace("/{variable}", "") + "/",
                    "");
            request.setAttribute(pathVariable, variable);
        }
    }

    private boolean checkParam(Map<String, String[]> webParam, String[] annoatationParam, String[] annotationVariable) {
        if (webParam.keySet().containsAll(Arrays.asList(annoatationParam))) {
            if (!(annoatationParam.length < annotationVariable.length)) {
                for (int i = 0; i < annotationVariable.length; i++) {
                    if (!webParam.get(annoatationParam[i])[0].equals(annotationVariable[i])) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean checkPath(String procPath, String methodPath, String webPath, boolean pathVariable) {
        String testPath = "^" + procPath
                + (pathVariable ? methodPath.replace("/{variable}", "") + "(\\/[\\w\\-_]{0,50}){1,1}$"
                        : methodPath + "$");
        return Pattern.compile(testPath).matcher(webPath).find();
    }
}