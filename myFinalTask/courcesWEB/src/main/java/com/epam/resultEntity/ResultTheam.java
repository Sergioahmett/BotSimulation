package com.epam.resultEntity;

import java.util.List;

import com.epam.daoLayer.daoEntity.DatabaseTheam;

public class ResultTheam {
    private DatabaseTheam theam;
    private List<ResultCourse> courseList;

    public ResultTheam(DatabaseTheam theam, List<ResultCourse> courseList) {
        this.setCourseList(courseList);
        this.theam = theam;
    }

    public DatabaseTheam getTheam() {
        return theam;
    }

    public void setTheam(DatabaseTheam theam) {
        this.theam = theam;
    }

    public List<ResultCourse> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<ResultCourse> courseList) {
        this.courseList = courseList;
    }
}
