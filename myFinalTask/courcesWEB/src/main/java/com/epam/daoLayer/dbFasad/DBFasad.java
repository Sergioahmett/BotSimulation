package com.epam.daoLayer.dbFasad;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import com.epam.daoLayer.abstractdao.AbstractDAO;
import com.epam.daoLayer.dao.*;
import com.epam.daoLayer.daoEntity.DatabaseCourse;
import com.epam.daoLayer.daoEntity.DatabaseSocial;
import com.epam.daoLayer.daoEntity.DatabaseTheam;
import com.epam.daoLayer.daoEntity.DatabaseUser;
import com.epam.interfaces.DatabaseFasadInterface;
import com.epam.resultEntity.ResultCourse;
import com.epam.resultEntity.ResultTeacher;
import com.epam.resultEntity.ResultTheam;
import com.epam.resultEntity.ResultUser;

import utils.CourseHelper;
import utils.Journal;

import static utils.CodeHandler.*;
import utils.ConnectionHendler;

public class DBFasad implements DatabaseFasadInterface {
    private CourseDAO courseDAO;
    private UserDAO userDAO;
    private SocialDAO socialDAO;
    private TheamDAO theamDAO;
    private CourseToUserDAO courseToUserDAO;
    private JournalDAO journalDAO;
    private static DatabaseFasadInterface instance;

    private DBFasad() {
        this.courseDAO = new CourseDAO();
        this.userDAO = new UserDAO();
        this.socialDAO = new SocialDAO();
        this.theamDAO = new TheamDAO();
        this.courseToUserDAO = new CourseToUserDAO();
        this.journalDAO = new JournalDAO();
    }

    public Map<String, Integer> getTheamForHeader() {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, theamDAO);
            List<DatabaseTheam> theamList = theamDAO.findAll();
            Map<String, Integer> result = new HashMap<>();
            theamList.forEach(theam -> {
                result.put(theam.getTitle(), theam.getId());
            });
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ResultTeacher getResultTeacherById(int id) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO, courseDAO, theamDAO, courseToUserDAO);
            return createResultTeacher(id);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<ResultTeacher> getResultTeacherList() {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO, courseDAO, theamDAO, courseToUserDAO);
            List<ResultTeacher> result = new LinkedList<>();
            List<DatabaseUser> teachers = userDAO.findAllTeachers();
            teachers.forEach(teacher -> result.add(createResultTeacher(teacher.getId())));
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int saveUser(DatabaseUser user) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO);
            if (userDAO.findUserByLogin(user.getLogin()) != null) {
                return LOGIN_ALREADY_EXIST;
            }
            if (userDAO.findUserByMail(user.getMail()) != null) {
                return MAIL_ALREADY_EXIST;
            }
            return userDAO.create(user) ? OK : ERROR;
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    public DatabaseUser getUserByLoginOrMail(String loginOrMail) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO);
            DatabaseUser user = userDAO.findUserByLogin(loginOrMail);
            if (user != null) {
                return user;
            }
            user = userDAO.findUserByMail(loginOrMail);
            if (user != null) {
                return user;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public DatabaseUser getUserByID(int id) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO);
            DatabaseUser user = userDAO.findUserById(id);
            if (user != null) {
                return user;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ResultCourse getResultCourseById(int id) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO, courseDAO, theamDAO, courseToUserDAO);
            DatabaseCourse course = courseDAO.findEntityById(id);
            ResultCourse result = null;
            if (course != null) {
                DatabaseUser teacher = userDAO.findUserById(course.getTeacher());
                DatabaseTheam theam = theamDAO.findTheamById(course.getTheme());
                int regStudents = courseToUserDAO.findAllCourseUsers(course.getId()).size();
                result = new ResultCourse(course, theam, teacher, regStudents);
            }
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int regOrUnregUserToCourses(int userId, int courseId, String param) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, courseToUserDAO, courseDAO, userDAO);
            DatabaseCourse course = courseDAO.findEntityById(courseId);
            DatabaseUser user = userDAO.findUserById(userId);
            if (course != null) {
                if (user != null) {
                    boolean register = courseToUserDAO.findByUserAndCourse(userId, courseId);
                    switch (param) {
                    case "register":
                        if (!register) {
                            int regCount = courseToUserDAO.findAllCourseUsers(courseId).size();
                            if (CourseHelper.checkActual(course)) {
                                if (regCount < course.getMaxStudentCount()) {
                                    if (courseToUserDAO.create(userId, courseId)) {
                                        return REGISTER_TO_COURSE_SUCCSESS;
                                    } else {
                                        return ERROR;
                                    }
                                } else {
                                    return COURSE_FULL;
                                }
                            } else {
                                return COURSE_NOT_ACTUAL;
                            }
                        } else {
                            return USER_ALREADY_REGISTER_TO_COURSE;
                        }
                    case "unregister":
                        if (register) {
                            if (courseToUserDAO.delete(userId, courseId)) {
                                return UNREGISTER_TO_COURSE_SUCCSESS;
                            } else {
                                return ERROR;
                            }
                        } else {
                            return USER_ALREADY_NOTREGISTER_TO_COURSE;
                        }
                    default:
                        return PARAMETER_NOT_VALID;
                    }
                } else {
                    return USER_NOT_FOUND;
                }
            } else {
                return COURSE_NOT_FOUND;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    public ResultUser getResultUserById(int id, String role) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO, courseDAO, theamDAO, courseToUserDAO, socialDAO);
            DatabaseUser user = userDAO.findUserById(id);
            if (user != null) {
                DatabaseSocial social = socialDAO.findUserById(id);
                List<DatabaseCourse> list;
                switch (role) {
                case "Teacher":
                    list = courseDAO.findAllByTeacherId(id);
                    break;
                case "Student":
                    list = courseDAO.findAllByUserId(id);
                    System.out.println(id + " " + role + " " + list.size());
                    break;
                default:
                    list = new LinkedList<>();
                }
                List<ResultCourse> resList = new LinkedList<>();
                list.forEach(course -> {
                    resList.add(new ResultCourse(course, theamDAO.findTheamById(course.getTheme()),
                            userDAO.findUserById(course.getTeacher()),
                            courseToUserDAO.findAllCourseUsers(course.getId()).size()));
                });
                ResultUser resultUser = new ResultUser();
                resultUser.setId(id);
                resultUser.setLogin(user.getLogin());
                resultUser.setFullName(user.getName() + " " + user.getSurname());
                resultUser.setMail(user.getMail());
                resultUser.setSocial(social);
                resultUser.setCourseList(resList);
                return resultUser;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<ResultCourse> getUserActualCourses(int id) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO, courseDAO, theamDAO, courseToUserDAO);
            List<Integer> courses = courseToUserDAO.findAllUserCourses(id);
            List<ResultCourse> result = new LinkedList<>();
            courses.forEach(courseId -> {
                DatabaseCourse course = courseDAO.findEntityById(courseId);
                if (CourseHelper.checkActual(course)) {
                    DatabaseUser teacher = userDAO.findUserById(course.getTeacher());
                    DatabaseTheam theam = theamDAO.findTheamById(course.getTheme());
                    int regStudents = courseToUserDAO.findAllCourseUsers(course.getId()).size();
                    result.add(new ResultCourse(course, theam, teacher, regStudents));
                }
            });
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ResultTheam getResultTheamById(int id) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO, courseDAO, theamDAO, courseToUserDAO);
            DatabaseTheam theam = theamDAO.findTheamById(id);
            if (theam != null) {
                List<ResultCourse> resultCoureList = new LinkedList<>();
                courseDAO.findAllByTheamId(theam.getId()).forEach(course -> {
                    resultCoureList.add(new ResultCourse(course, theam, userDAO.findUserById(course.getTeacher()),
                            courseToUserDAO.findAllCourseUsers(course.getId()).size()));
                });
                return new ResultTheam(theam, resultCoureList);
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<ResultCourse> getResultCourseList() {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO, courseDAO, theamDAO, courseToUserDAO);
            List<DatabaseCourse> list = courseDAO.findAll();
            List<ResultCourse> resultList = new LinkedList<>();
            list.forEach(course -> {
                resultList.add(new ResultCourse(course, theamDAO.findTheamById(course.getTheme()),
                        userDAO.findUserById(course.getTeacher()),
                        courseToUserDAO.findAllCourseUsers(course.getId()).size()));
            });
            return resultList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<ResultTheam> getResultTheamList() {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO, courseDAO, theamDAO, courseToUserDAO);
            List<DatabaseTheam> list = theamDAO.findAll();
            List<ResultTheam> resultList = new LinkedList<>();
            list.forEach(theam -> {
                List<ResultCourse> resultCoureList = new LinkedList<>();
                courseDAO.findAllByTheamId(theam.getId()).forEach(course -> {
                    resultCoureList.add(new ResultCourse(course, theam, userDAO.findUserById(course.getId()),
                            courseToUserDAO.findAllCourseUsers(course.getId()).size()));
                });
                resultList.add(new ResultTheam(theam, resultCoureList));
            });
            return resultList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int changeCourse(DatabaseCourse course) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, courseDAO);
            DatabaseCourse currentCourse = courseDAO.findEntityById(course.getId());
            if (currentCourse != null) {
                return courseDAO.update(course) ? COURSE_CHANGED : ERROR;
            } else {
                return COURSE_NOT_FOUND;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    public int createCourse(DatabaseCourse course) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, courseDAO);
            return courseDAO.create(course) ? COURSE_CREATED : ERROR;
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    public int deleteCourse(int courseId) {
        try (Connection con = getConnection(); ConnectionHendler ch = new ConnectionHendler(con, false);) {
            setConnectionToDAO(con, courseDAO, courseToUserDAO, journalDAO);
            if (courseDAO.findEntityById(courseId) != null) {
                if (courseDAO.delete(courseId) && courseToUserDAO.deleteByCourseId(courseId)
                        && journalDAO.delete(courseId)) {
                    ch.commit();
                    return COURSE_DELETED;
                }
                return ERROR;
            } else {
                return COURSE_NOT_FOUND;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    public boolean updateUserPass(DatabaseUser user) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO);
            return userDAO.update(user);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<DatabaseUser> getAllUser() {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO);
            return userDAO.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public DatabaseCourse getCourseById(int id) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, courseDAO);
            return courseDAO.findEntityById(id);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<DatabaseTheam> getAllTheams() {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, theamDAO);
            return theamDAO.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<DatabaseUser> getAllStudents() {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO);
            return userDAO.findAllStudents();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<DatabaseUser> getAllTeachers() {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO);
            return userDAO.findAllTeachers();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int doTeacherById(int id, String description) {
        try (Connection con = getConnection(); ConnectionHendler ch = new ConnectionHendler(con, false)) {
            setConnectionToDAO(con, userDAO, courseToUserDAO);
            DatabaseUser user = userDAO.findUserById(id);
            if (user != null) {
                if (user.getRole().equals("Student")) {
                    user.getBuilder().setRole("Teacher");
                    courseToUserDAO.deleteByUserId(id);
                    if (userDAO.update(user) && userDAO.createTeacherDescription(id, description)) {
                        ch.commit();
                        return USER_SET_TO_TEACHER;
                    } else {
                        return ERROR;
                    }
                } else {
                    return USER_ALREADY_TEACHER;
                }
            } else {
                return USER_NOT_FOUND;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    public int changeTeacherDescription(int id, String description) {
        try (Connection con = getConnection(); ConnectionHendler ch = new ConnectionHendler(con, false)) {
            setConnectionToDAO(con, userDAO, courseToUserDAO);
            DatabaseUser user = userDAO.findUserById(id);
            if (user != null) {
                if (user.getRole().equals("Teacher")) {
                    if (userDAO.setTeacherDescription(id, description)) {
                        ch.commit();
                        return DESCRIPTION_SET;
                    } else {
                        return ERROR;
                    }
                } else {
                    return USER_NOT_TEACHER;
                }
            } else {
                return USER_NOT_FOUND;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    public int doStudentById(int id) {
        try (Connection con = getConnection(); ConnectionHendler ch = new ConnectionHendler(con, false)) {
            setConnectionToDAO(con, userDAO, courseDAO);
            DatabaseUser user = userDAO.findUserById(id);
            if (user != null) {
                if (user.getRole().equals("Teacher")) {
                    user.getBuilder().setRole("Student");
                    List<DatabaseCourse> list = courseDAO.findAllByTeacherId(id);
                    list.forEach(course -> {
                        course.getBuilder().setTeacher(-1);
                        courseDAO.update(course);
                    });
                    if (userDAO.update(user) && userDAO.deleteTeacherDecription(id)) {
                        ch.commit();
                        return USER_SET_TO_STUDENT;
                    } else {
                        return ERROR;
                    }
                } else {
                    return USER_ALREADY_STUDENT;
                }
            } else {
                return USER_NOT_FOUND;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    public int blockUser(int id) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO);
            DatabaseUser user = userDAO.findUserById(id);
            if (user != null) {
                if (!user.getRole().equals("Admin")) {
                    if (!user.isBlock()) {
                        user.getBuilder().setBlock(true);
                        return userDAO.update(user) ? USER_SET_TO_BLOCK : ERROR;
                    } else {
                        return USER_ALREADY_BLOCKED;
                    }
                } else {
                    return USER_ADMIN;
                }
            } else {
                return USER_NOT_FOUND;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    public int unblockUser(int id) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO);
            DatabaseUser user = userDAO.findUserById(id);
            if (user != null) {
                if (user.isBlock()) {
                    user.getBuilder().setBlock(false);
                    return userDAO.update(user) ? USER_UNBLOCK : ERROR;
                } else {
                    return USER_NOT_BLOCK;
                }
            } else {
                return USER_NOT_FOUND;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    public static DatabaseFasadInterface getInstance() {
        if (instance == null) {
            instance = new DBFasad();
        }
        return instance;
    }

    private static Connection getConnection() throws SQLException {
        ResourceBundle resource = ResourceBundle.getBundle("database");
        Properties props = new Properties();
        String url = resource.getString("db.url");
        props.setProperty("user", resource.getString("db.user"));
        props.setProperty("password", resource.getString("db.password"));
        props.setProperty("useSSL", resource.getString("db.useSSL"));
        props.setProperty("useLegacyDatetimeCode", resource.getString("db.useLegacyDatetimeCode"));
        props.setProperty("serverTimezone", resource.getString("db.serverTimezone"));
        return DriverManager.getConnection(url, props);
    }

    private ResultTeacher createResultTeacher(int id) {
        DatabaseUser teacher = userDAO.findUserById(id);
        if (teacher != null) {
            String descr = userDAO.getTeacherDescription(id);
            List<DatabaseCourse> courses = courseDAO.findAllByTeacherId(id);
            List<ResultCourse> result = new LinkedList<>();
            courses.forEach(course -> {
                DatabaseTheam theam = theamDAO.findTheamById(course.getTheme());
                int regStudents = courseToUserDAO.findAllCourseUsers(course.getId()).size();
                result.add(new ResultCourse(course, theam, teacher, regStudents));
            });
            return new ResultTeacher(id, teacher.getName() + " " + teacher.getSurname(), descr, result);
        } else {
            return null;
        }
    }

    // public static Connection getVideobaseConnection() throws SQLException,
    // NamingException {
    // Context contx = (Context) (new InitialContext().lookup("java:comp/env"));
    // DataSource ds = (DataSource) contx.lookup("jdbc/cources");
    // return ds.getConnection();
    // }

    @SuppressWarnings("rawtypes")
    private void setConnectionToDAO(Connection con, AbstractDAO... dao) {
        for (AbstractDAO abstractDAO : dao) {
            abstractDAO.setConnection(con);
        }

    }

    @Override
    public int changeTheam(DatabaseTheam databaseTheam) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, theamDAO);
            if (theamDAO.findTheamById(databaseTheam.getId()) != null) {
                return theamDAO.update(databaseTheam) ? THEAM_CHANGED : ERROR;
            } else {
                return THEAM_NOT_FOUND;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    @Override
    public int createTheam(DatabaseTheam databaseTheam) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, theamDAO);
            return theamDAO.create(databaseTheam) ? THEAM_CREATED : ERROR;
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    @Override
    public int deleteTheam(int theamId) {
        try (Connection con = getConnection(); ConnectionHendler ch = new ConnectionHendler(con, false);) {
            setConnectionToDAO(con, courseDAO, theamDAO);
            if (theamDAO.findTheamById(theamId) != null) {
                List<DatabaseCourse> courses = courseDAO.findAllByTheamId(theamId);
                for (DatabaseCourse course : courses) {
                    course.getBuilder().setTheme(-1);
                    courseDAO.update(course);
                }
                if (theamDAO.delete(theamId)) {
                    ch.commit();
                    return THEAM_DELETED;
                }
                return ERROR;
            } else {
                return COURSE_NOT_FOUND;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ERROR;
        }
    }

    @Override
    public boolean checkAccessToJournal(int userId, int courseId, String role) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, courseDAO, courseToUserDAO);
            switch (role) {
            case "Teacher":
                DatabaseCourse course = courseDAO.findEntityById(courseId);
                return course != null && course.getTeacher() == userId;
            case "Student":
                List<Integer> list = courseToUserDAO.findAllUserCourses(userId);
                return list.contains(courseId);
            default:
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Journal getJournal(int courseId, String role) {
        try (Connection con = getConnection();) {
            setConnectionToDAO(con, userDAO, courseDAO, journalDAO, courseToUserDAO);
            Journal journal = journalDAO.getById(courseId);
            if (journal == null && role.equals("Teacher")) {
                DatabaseCourse course = courseDAO.findEntityById(courseId);
                if (CourseHelper.checkCurrent(course) || CourseHelper.checkArchive(course)) {
                    journal = new Journal();
                    List<DatabaseUser> userList = new LinkedList<>();
                    courseToUserDAO.findAllCourseUsers(courseId).forEach(id -> {
                        userList.add(userDAO.findUserById(id));
                    });
                    journal.addStudents(userList);
                    return journalDAO.create(courseId, journal) ? journal : null;
                } else {
                    return null;
                }
            } else {
                return journal;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
