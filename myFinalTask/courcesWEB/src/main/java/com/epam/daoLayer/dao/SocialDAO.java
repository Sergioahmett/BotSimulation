package com.epam.daoLayer.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import com.epam.daoLayer.abstractdao.AbstractDAO;
import com.epam.daoLayer.daoEntity.DatabaseSocial;

public class SocialDAO extends AbstractDAO<DatabaseSocial> {
    private final static String SQL_FIND_ALL = "SELECT * FROM cources.social";
    private final static String SQL_INSERT = "INSERT INTO cources.social (id, facebookId, twitterId, googleId) VALUES (?, ?, ?, ?);";
    private final static String SQL_UPDATE = "UPDATE cources.social SET facebookId = ?, twitterId = ?, googleId = WHERE (id = ?)";
    private final static String SQL_FIND_BY_ID = "SELECT * FROM cources.social WHERE id = ";
    private final static String SQL_FIND_BY_FACEBOOK = "SELECT * FROM cources.social WHERE facebookId = ";
    private final static String SQL_FIND_BY_TWITTER = "SELECT * FROM cources.social WHERE twitterId = ";
    private final static String SQL_FIND_BY_GOOGLE = "SELECT * FROM cources.social WHERE googleId = ";
    private final static String SQL_DELL_BY_ID = "DELETE FROM cources.social WHERE id = ?";

    public List<DatabaseSocial> findAll() {
        return findBySQL(SQL_FIND_ALL);
    }

    public DatabaseSocial findUserById(int id) {
        List<DatabaseSocial> list = findBySQL(SQL_FIND_BY_ID + id);
        return list.size() > 0 ? list.get(0) : null;
    }

    public DatabaseSocial findUserByFacebookId(String facebookId) {
        List<DatabaseSocial> list = findBySQL(SQL_FIND_BY_FACEBOOK + facebookId);
        return list.size() > 0 ? list.get(0) : null;
    }

    public DatabaseSocial findUserByTwitterId(String twitterId) {
        List<DatabaseSocial> list = findBySQL(SQL_FIND_BY_TWITTER + twitterId);
        return list.size() > 0 ? list.get(0) : null;
    }

    public DatabaseSocial findUserByGoogleId(String googleId) {
        List<DatabaseSocial> list = findBySQL(SQL_FIND_BY_GOOGLE + googleId);
        return list.size() > 0 ? list.get(0) : null;
    }

    public boolean delete(String id) {
        try (PreparedStatement st = connection.prepareStatement(SQL_DELL_BY_ID);) {
            st.setString(1, id);
            if (st.executeUpdate() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Error by creating statment!");
        }
        return false;
    }

    public boolean create(DatabaseSocial entity) {
        try (PreparedStatement st = connection.prepareStatement(SQL_INSERT)) {
            st.setInt(1, entity.getId());
            st.setString(2, entity.getFacebookId());
            st.setString(3, entity.getTwitterId());
            st.setString(4, entity.getGoogleId());
            if (st.executeUpdate() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Error by creating statment!");
        }
        return false;
    }

    public boolean update(DatabaseSocial entity) {
        try (PreparedStatement st = connection.prepareStatement(SQL_UPDATE)) {
            st.setInt(4, entity.getId());
            st.setString(1, entity.getFacebookId());
            st.setString(2, entity.getTwitterId());
            st.setString(3, entity.getGoogleId());
            return st.executeUpdate() > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Error by creating statment!");
            return false;
        }
    }

    private List<DatabaseSocial> findBySQL(String sql) {
        try (Statement st = connection.createStatement()) {
            ResultSet rs = st.executeQuery(sql);
            List<DatabaseSocial> result = new LinkedList<>();
            while (rs.next()) {
                DatabaseSocial social = new DatabaseSocial();
                social.setId(rs.getInt("id"));
                social.setFacebookId(rs.getString("facebookId"));
                social.setTwitterId(rs.getString("twitterId"));
                social.setGoogleId(rs.getString("googleId"));
                result.add(social);
            }
            return result;
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("Error by creating statment!");
        }
        return null;
    }
}
