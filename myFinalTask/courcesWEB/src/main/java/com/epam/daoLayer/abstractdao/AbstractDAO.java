package com.epam.daoLayer.abstractdao;

import java.sql.Connection;
import java.sql.SQLException;

import com.epam.daoLayer.daoEntity.BaseEntity;

public abstract class AbstractDAO<T extends BaseEntity> {
    protected Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }
}
