package com.epam.processor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.annotation.Processor;
import com.epam.interfaces.ProcessorIntarface;

@Processor(path = "/error")
public class ErrorProcessor implements ProcessorIntarface {
    private static ErrorProcessor instance;

    private ErrorProcessor() {
    }

    public static ErrorProcessor getInstance() {
        if (instance == null) {
            instance = new ErrorProcessor();
        }
        return instance;
    }
    @Override
    public void startProcess(HttpServletRequest request, HttpServletResponse response) {
    }

    public void getErrorPage(HttpServletRequest request, HttpServletResponse response, int errorCode) {

    }
}
