package com.epam.processor;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.annotation.HTTPMethod;
import com.epam.annotation.MethodAnnotationController;
import com.epam.annotation.Processor;
import com.epam.annotation.RequestMappin;
import com.epam.daoLayer.dbFasad.DBFasad;
import com.epam.enums.HttpMethod;
import com.epam.interfaces.DatabaseFasadInterface;
import com.epam.interfaces.ProcessorIntarface;
import com.epam.resultEntity.ResultTeacher;

import static utils.CodeHandler.*;

@Processor(path = "/teachers")
public class TeacherProcessor implements ProcessorIntarface {
    private DatabaseFasadInterface dbFacade = DBFasad.getInstance();
    private static TeacherProcessor instance;

    private TeacherProcessor() {
    }

    public static TeacherProcessor getInstance() {
        if (instance == null) {
            instance = new TeacherProcessor();
        }
        return instance;
    }
    @Override
    public void startProcess(HttpServletRequest request, HttpServletResponse response) {
        new MethodAnnotationController().redirectToMethod(this, request, response);
    }
    @HTTPMethod(method = HttpMethod.GET)
    @RequestMappin(path = "/{variable}", pathVariable = "teacherId")
    public void getTeacherPage(HttpServletRequest request, HttpServletResponse response) {
        try {
            int teacherId = Integer.parseInt((String) request.getAttribute("teacherId"));
            ResultTeacher teacher = dbFacade.getResultTeacherById(teacherId);
            if (teacher != null) {
                List<ResultTeacher> teachers = new LinkedList<>();
                teachers.add(teacher);
                request.setAttribute("teachers", teachers);
                request.getRequestDispatcher("/teachers.jsp").forward(request, response);
            } else {
                request.setAttribute("error", TEACHER_NOT_FOUND);
                getTeachersPage(request, response);
            }
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        } 
    }
    @HTTPMethod(method = HttpMethod.GET)
    @RequestMappin
    public void getTeachersPage(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<ResultTeacher> teachers = dbFacade.getResultTeacherList();
            request.setAttribute("teachers", teachers);
            request.getRequestDispatcher("/teachers.jsp").forward(request, response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

}
