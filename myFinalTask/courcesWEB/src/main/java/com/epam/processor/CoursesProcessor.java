package com.epam.processor;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.annotation.HTTPMethod;
import com.epam.annotation.MethodAnnotationController;
import com.epam.annotation.Processor;
import com.epam.annotation.RequestMappin;
import com.epam.daoLayer.dbFasad.DBFasad;
import com.epam.enums.HttpMethod;
import com.epam.interfaces.DatabaseFasadInterface;
import com.epam.interfaces.ProcessorIntarface;
import com.epam.resultEntity.ResultCourse;

import utils.CourseHelper;
import static utils.CodeHandler.*;

@Processor(path = "/courses")
public class CoursesProcessor implements ProcessorIntarface {
    private DatabaseFasadInterface dbFacade = DBFasad.getInstance();
    private static CoursesProcessor instance;

    private CoursesProcessor() {
    }

    public static CoursesProcessor getInstance() {
        if (instance == null) {
            instance = new CoursesProcessor();
        }
        return instance;
    }
    @Override
    public void startProcess(HttpServletRequest request, HttpServletResponse response) {
        new MethodAnnotationController().redirectToMethod(this, request, response);

    }

    @SuppressWarnings("unchecked")
    @HTTPMethod(method = HttpMethod.POST)
    @RequestMappin(path = "/{variable}", pathVariable = "courseId", requiredParam = "command")
    public void userOperationWithCourse(HttpServletRequest request, HttpServletResponse response) {
        try {
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute("role") != null) {
                if (session.getAttribute("role").equals("Student")) {
                    int courseId = Integer.parseInt((String) request.getAttribute("courseId"));
                    int code = dbFacade.regOrUnregUserToCourses((Integer) session.getAttribute("userId"), courseId,
                            request.getParameter("command"));
                    if (code == REGISTER_TO_COURSE_SUCCSESS || code == UNREGISTER_TO_COURSE_SUCCSESS) {
                        List<ResultCourse> list = (List<ResultCourse>) session.getAttribute("courseList");
                        ResultCourse course = dbFacade.getResultCourseById(courseId);
                        if (code == UNREGISTER_TO_COURSE_SUCCSESS) {
                            list.remove(course);
                        } else {
                            list.add(course);
                        }
                        request.setAttribute("courseList", list);
                    }
                    request.setAttribute("msg", code);
                    returnCorse(request, response);
                } else {
                    request.setAttribute("msg", ONLY_STUDENTS_CAN_RAGISTER);
                    returnCorse(request, response);
                }
            } else {
                response.sendRedirect("http://localhost:8080/auth/login");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @HTTPMethod(method = HttpMethod.GET)
    @RequestMappin
    public void returnCorsesList(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.setAttribute("resultList", getResultListByParam(request.getParameter("show")));
            request.getRequestDispatcher("/courses.jsp").forward(request, response);
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
    }
    @HTTPMethod(method = HttpMethod.GET)
    @RequestMappin(path = "/{variable}", pathVariable = "courseId")
    public void returnCorse(HttpServletRequest request, HttpServletResponse response) {
        try {
            int courseId = Integer.parseInt((String) request.getAttribute("courseId"));
            request.setAttribute("resultCourse", dbFacade.getResultCourseById(courseId));
            request.getRequestDispatcher("/course.jsp").forward(request, response);
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
    }

    private List<ResultCourse> getResultListByParam(String param) {
        CourseHelper<ResultCourse> helper = new CourseHelper<>();
        switch (param != null ? param : "") {
        default:
        case "all":
            return dbFacade.getResultCourseList();
        case "actual":
            return helper.getActualCourses(dbFacade.getResultCourseList());
        case "current":
            return helper.getCurrentCourses(dbFacade.getResultCourseList());
        case "archive":
            return helper.getArchiveCourses(dbFacade.getResultCourseList());
        }
    }
}
