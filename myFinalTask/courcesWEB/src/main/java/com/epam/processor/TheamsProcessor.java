package com.epam.processor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.annotation.HTTPMethod;
import com.epam.annotation.MethodAnnotationController;
import com.epam.annotation.Processor;
import com.epam.annotation.RequestMappin;
import com.epam.daoLayer.dbFasad.DBFasad;
import com.epam.enums.HttpMethod;
import com.epam.interfaces.DatabaseFasadInterface;
import com.epam.interfaces.ProcessorIntarface;
import com.epam.resultEntity.ResultTheam;

import static utils.CodeHandler.*;

@Processor(path = "/theams")
public class TheamsProcessor implements ProcessorIntarface {
    private DatabaseFasadInterface dbFacade = DBFasad.getInstance();
    private static TheamsProcessor instance;

    private TheamsProcessor() {
    }

    public static TheamsProcessor getInstance() {
        if (instance == null) {
            instance = new TheamsProcessor();
        }
        return instance;
    }
    @Override
    public void startProcess(HttpServletRequest request, HttpServletResponse response) {
        new MethodAnnotationController().redirectToMethod(this, request, response);
    }

    @HTTPMethod(method = HttpMethod.GET)
    @RequestMappin(path = "/{variable}", pathVariable = "theamId")
    public void returnTheam(HttpServletRequest request, HttpServletResponse response) {
        try {
            int theamId = Integer.parseInt((String) request.getAttribute("theamId"));
            ResultTheam theam = dbFacade.getResultTheamById(theamId);
            if (theam != null) {
                request.setAttribute("theam", theam);
                request.getRequestDispatcher("/theam.jsp").forward(request, response);
            } else {
                request.setAttribute("error", THEAM_NOT_FOUND);
                returnTheamList(request, response);
            }
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
    }

    @HTTPMethod(method = HttpMethod.GET)
    @RequestMappin(path = "/theams")
    public void returnTheamList(HttpServletRequest request, HttpServletResponse response) {
        try {
            request.setAttribute("resultList", dbFacade.getResultTheamList());
            request.getRequestDispatcher("/theams.jsp").forward(request, response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

}
