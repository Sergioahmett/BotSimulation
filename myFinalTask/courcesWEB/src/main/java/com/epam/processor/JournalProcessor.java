package com.epam.processor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.annotation.HTTPMethod;
import com.epam.annotation.MethodAnnotationController;
import com.epam.annotation.Processor;
import com.epam.annotation.RequestMappin;
import com.epam.daoLayer.dbFasad.DBFasad;
import com.epam.enums.HttpMethod;
import com.epam.interfaces.DatabaseFasadInterface;
import com.epam.interfaces.ProcessorIntarface;

import utils.Journal;
import static utils.CodeHandler.*;

@Processor(path = "/journal")
public class JournalProcessor implements ProcessorIntarface {
    private DatabaseFasadInterface dbFacade = DBFasad.getInstance();
    private static JournalProcessor instance;

    private JournalProcessor() {
    }

    public static JournalProcessor getInstance() {
        if (instance == null) {
            instance = new JournalProcessor();
        }
        return instance;
    }

    @Override
    public void startProcess(HttpServletRequest request, HttpServletResponse response) {
        new MethodAnnotationController().redirectToMethod(this, request, response);
    }

    @HTTPMethod(method = HttpMethod.GET)
    @RequestMappin(path = "/{variable}", pathVariable = "courseId")
    public void getJournalPage(HttpServletRequest request, HttpServletResponse response) {
        try {
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute("role") != null) {
                if (dbFacade.checkAccessToJournal((int) session.getAttribute("userId"),
                        Integer.parseInt((String) request.getAttribute("courseId")),
                        (String) session.getAttribute("role"))) {
                    Journal j = dbFacade.getJournal(Integer.parseInt((String) request.getAttribute("courseId")),
                            (String) session.getAttribute("role"));
                    if (j != null) {
                        request.setAttribute("journal", j);
                        request.getRequestDispatcher("/journal.jsp").forward(request, response);
                    } else {
                        request.setAttribute("error", JOURNAL_NOT_FOUND);
                        UserProcessor.getInstance().getProfilePage(request, response);
                    }
                } else {
                    request.setAttribute("error", ACCESS_DENIED);
                    UserProcessor.getInstance().getProfilePage(request, response);
                }
            } else {
                response.sendRedirect("http://localhost:8080");
            }
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
    }

    @HTTPMethod(method = HttpMethod.POST)
    @RequestMappin(path = "/{variable}", pathVariable = "courseId", requiredParam = {})
    public void logoutUser(HttpServletRequest request, HttpServletResponse response) {
        try {
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute("role") != null) {
                if (dbFacade.checkAccessToJournal((int) session.getAttribute("userId"),
                        Integer.parseInt((String) request.getAttribute("courseId")),
                        (String) session.getAttribute("role"))) {
                    Journal j = dbFacade.getJournal(Integer.parseInt((String) request.getAttribute("courseId")),
                            (String) session.getAttribute("role"));
                    if (j != null) {
                        request.setAttribute("journal", j);
                        request.getRequestDispatcher("/journal.jsp").forward(request, response);
                    } else {
                        request.setAttribute("error", JOURNAL_NOT_FOUND);
                        UserProcessor.getInstance().getProfilePage(request, response);
                    }
                } else {
                    request.setAttribute("error", ACCESS_DENIED);
                    UserProcessor.getInstance().getProfilePage(request, response);
                }
            } else {
                response.sendRedirect("http://localhost:8080");
            }
        } catch (IOException | ServletException e) {
            e.printStackTrace();
        }
    }
}
