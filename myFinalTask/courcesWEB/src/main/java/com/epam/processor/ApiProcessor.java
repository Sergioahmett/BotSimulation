package com.epam.processor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.annotation.Processor;
import com.epam.interfaces.ProcessorIntarface;

@Processor(path = "/api")
public class ApiProcessor implements ProcessorIntarface {

    @Override
    public void startProcess(HttpServletRequest request, HttpServletResponse response) {

    }

}
